# coding: utf-8

import os
import logging
from enum import Enum, auto
from collections import namedtuple
from typing import Tuple, List
from queue import Queue
from threading import Thread

"""
# --- Day 11: Space Police ---

On the way to Jupiter, you're [pulled over](https://www.youtube.com/watch?v=KwY28rpyKDE) by the _Space Police_.

"Attention, unmarked spacecraft! You are in violation of Space Law! All spacecraft must have a clearly visible _registration identifier_! You have 24 hours to comply or be sent to [Space Jail](https://www.youtube.com/watch?v=BVn1oQL9sWg&amp;t=5)!"

Not wanting to be sent to Space Jail, you radio back to the Elves on Earth for help. Although it takes almost three hours for their reply signal to reach you, they send instructions for how to power up the _emergency hull painting robot_ and even provide a small [Intcode program](9) (your puzzle input) that will cause it to paint your ship appropriately.

There's just one problem: you don't have an emergency hull painting robot.

You'll need to build a new emergency hull painting robot. The robot needs to be able to move around on the grid of square panels on the side of your ship, detect the color of its current panel, and paint its current panel _black_ or _white_. (All of the panels are currently _black_.)

The Intcode program will serve as the brain of the robot. The program uses input instructions to access the robot's camera: provide `` 0 `` if the robot is over a _black_ panel or `` 1 `` if the robot is over a _white_ panel. Then, the program will output two values:

*   First, it will output a value indicating the _color to paint the panel_ the robot is over: `` 0 `` means to paint the panel _black_, and `` 1 `` means to paint the panel _white_.
*   Second, it will output a value indicating the _direction the robot should turn_: `` 0 `` means it should turn _left 90 degrees_, and `` 1 `` means it should turn _right 90 degrees_.

After the robot turns, it should always move _forward exactly one panel_. The robot starts facing _up_.

The robot will continue running for a while like this and halt when it is finished drawing. Do not restart the Intcode computer inside the robot during this process.

For example, suppose the robot is about to start running. Drawing black panels as `` . ``, white panels as `` # ``, and the robot pointing the direction it is facing (`` &lt; ^ &gt; v ``), the initial state and region near the robot looks like this:

    .....
    .....
    ..^..
    .....
    .....

The panel under the robot (not visible here because a `` ^ `` is shown instead) is also black, and so any input instructions at this point should be provided `` 0 ``. Suppose the robot eventually outputs `` 1 `` (paint white) and then `` 0 `` (turn left). After taking these actions and moving forward one panel, the region now looks like this:

    .....
    .....
    .&lt;#..
    .....
    .....

Input instructions should still be provided `` 0 ``. Next, the robot might output `` 0 `` (paint black) and then `` 0 `` (turn left):

    .....
    .....
    ..#..
    .v...
    .....

After more outputs (`` 1,0 ``, `` 1,0 ``):

    .....
    .....
    ..^..
    .##..
    .....

The robot is now back where it started, but because it is now on a white panel, input instructions should be provided `` 1 ``. After several more outputs (`` 0,1 ``, `` 1,0 ``, `` 1,0 ``), the area looks like this:

    .....
    ..&lt;#.
    ...#.
    .##..
    .....

Before you deploy the robot, you should probably have an estimate of the area it will cover: specifically, you need to know the _number of panels it paints at least once_, regardless of color. In the example above, the robot painted _`` 6 `` panels_ at least once. (It painted its starting panel twice, but that panel is [still only counted once](https://www.youtube.com/watch?v=KjsSvjA5TuE); it also never painted the panel it ended on.)

Build a new emergency hull painting robot and run the Intcode program on it. _How many panels does it paint at least once?_

--- Part Two ---
You're not sure what it's trying to paint, but it's definitely not a registration identifier. The Space Police are getting impatient.

Checking your external ship cameras again, you notice a white panel marked "emergency hull painting robot starting panel". The rest of the panels are still black, but it looks like the robot was expecting to start on a white panel, not a black one.

Based on the Space Law Space Brochure that the Space Police attached to one of your windows, a valid registration identifier is always eight capital letters. After starting the robot on a single white panel instead, what registration identifier does it paint on your hull?
"""


def debug(func):
    """Debug utility function."""
    def wrapper(*args):
        if os.getenv("DEBUG"):
            logging.debug(f"Registers: {args[0].registers}")
            logging.debug(f"Program: {args[0].program[:20]}")
        return func(*args)
    return wrapper


class Register:

    PC = 0
    EIP = 0
    EBP = 0

    def __repr__(self):
        return f"PC: {self.PC}, EIP: {self.EIP}, EBP: {self.EBP}"


class Program(Thread):

    def __init__(self, data: str, inputs: Queue, outputs: Queue, memory_size: int = 1024):
        super(Program, self).__init__()
        self.data = data
        self.status = "idle"
        self.program = [int(instruction) for instruction in self.data.split(',')]
        self.inputs = inputs
        self.outputs = outputs
        self.memory_size = memory_size
        self.registers = Register()
        self.__alloc_memory()

    def __alloc_memory(self) -> None:
        """Expended program memory space for futher data allocation"""

        for _ in range(self.memory_size):
            self.program.append(0)

    def run(self) -> None:
        """Start program execution """

        logging.debug(f"Allocate additional memory of size: {self.memory_size}, program: {self.data}")
        logging.debug(f"Start program!")
        self.status = "running"

        while True:
            try:
                opcode, modes = self.__parse_current_instruction()
                self.__execute_opcode(opcode, modes)
            except StopIteration:
                break

        logging.debug(f"End program execution: output: {self.outputs.qsize()}")

    def __halt(self) -> Exception:
        self.status = "halt"
        raise StopIteration("Halt program: opcode: 99")

    def __load_r(self, mode: int, addr: int) -> int:
        """Load instruction parameters, for read parameters modes are:
        0: immediate => read value pointed at the address of the paremeter
        1: position => value of the parameter is the parameter
        2: relative => read value pointed at the address of the parameter + EBP"""

        if mode == 0:
            return self.program[self.program[addr]]
        elif mode == 1:
            return self.program[addr]
        elif mode == 2:
            return self.program[self.registers.EBP + self.program[addr]]
        else:
            raise ValueError(f"Unknown mode:{mode}!")

    def __load_w(self, mode: int, index: int) -> int:
        """Load instruction parameters, for write parameters, modes are:
        0: immediate => read value pointed at the address of the paremeter
        2: relative => read value pointed at the address of the parameter + EBP"""

        if mode == 0:
            return self.program[index]
        elif mode == 2:
            return self.program[index] + self.registers.EBP
        else:
            raise ValueError(f"Unknown mode:{mode}!")

    def __store(self, value: int, x_addr: int) -> None:
        """Store value at the given address of the program."""

        self.program[x_addr] = value

    def __add(self, x: int, y: int, dest: int) -> None:
        """opcode: `01`, PC: `4`, add x and y and store the value in dest."""
        self.__store(x + y, dest)
        self.registers.EIP += 4

    def __mul(self, x: int, y: int, dest: int) -> None:
        """opcode: `02`, PC: `4`, multiply x and y and store the value in dest."""

        self.__store(x * y, dest)
        self.registers.EIP += 4

    def stdin(self, dest: int) -> None:
        """opcode: `03`, PC: `2`, get stdin and store it to dest."""

        self.__store(int(self.inputs.get()), dest)
        self.registers.EIP += 2

    def stdout(self, value: int) -> None:
        """opcode: `04`, PC: `2`, stdout value."""

        self.outputs.put(value)
        self.registers.EIP += 2
        logging.debug(f">&1 {value}")

    def __jz(self, x: int, addr: int) -> None:
        """opcode: `05`, PC: `3`, jump to addr if x is non-zero else it does nothing."""

        if x:
            self.registers.EIP = addr
        else:
            self.registers.EIP += 3

    def __jnz(self, x: int, addr: int) -> None:
        """opcode: `06`, PC: `3`, jump to addr if x is zero else it does nothing."""

        if not x:
            self.registers.EIP = addr
        else:
            self.registers.EIP += 3

    def __lt(self, x: int, y: int, dest: int) -> None:
        """opcode: `07`, PC: `4`, Compare if x lower then y write `1` to dest else `0`."""

        if x < y:
            self.__store(1, dest)
        else:
            self.__store(0, dest)
        self.registers.EIP += 4

    def __eq(self, x: int, y: int, dest: int) -> None:
        """opcode: `08`, PC: `4` if x equal to y then write `1` to dest else `0`."""

        if x == y:
            self.__store(1, dest)
        else:
            self.__store(0, dest)

        self.registers.EIP += 4

    def __sb(self, offset: int) -> None:
        """opcode: `09`, Move register `EBP` by the value of offset (could be negative)."""

        self.registers.EBP += offset
        self.registers.EIP += 2

    def __parse_current_instruction(self) -> Tuple[int, List[int]]:
        """Parse current instruction."""

        instruction = self.program[self.registers.EIP]
        opcode = instruction % 100
        modes = [
            instruction // 100 % 10,
            instruction // 1000 % 10,
            instruction // 10000 % 10
        ]

        return opcode, modes

    @debug
    def __execute_opcode(self, opcode: int, modes: list) -> None:
        """Execute opcode """

        logging.debug(f"Going to execute opcode: {opcode}")

        if opcode == 1:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            y = self.__load_r(modes[1], self.registers.EIP + 2)
            dest = self.__load_w(modes[2], self.registers.EIP + 3)
            self.__add(x, y, dest)
        elif opcode == 2:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            y = self.__load_r(modes[1], self.registers.EIP + 2)
            dest = self.__load_w(modes[2], self.registers.EIP + 3)
            self.__mul(x, y, dest)
        elif opcode == 3:
            dest = self.__load_w(modes[0], self.registers.EIP + 1)
            self.stdin(dest)
        elif opcode == 4:
            value = self.__load_r(modes[0], self.registers.EIP + 1)
            self.stdout(value)
        elif opcode == 5:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            addr = self.__load_r(modes[1], self.registers.EIP + 2)
            self.__jz(x, addr)
        elif opcode == 6:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            addr = self.__load_r(modes[1], self.registers.EIP + 2)
            self.__jnz(x, addr)
        elif opcode == 7:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            y = self.__load_r(modes[1], self.registers.EIP + 2)
            dest = self.__load_w(modes[2], self.registers.EIP + 3)
            self.__lt(x, y, dest)
        elif opcode == 8:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            y = self.__load_r(modes[1], self.registers.EIP + 2)
            dest = self.__load_w(modes[2], self.registers.EIP + 3)
            self.__eq(x, y, dest)
        elif opcode == 9:
            offset = self.__load_r(modes[0], self.registers.EIP + 1)
            self.__sb(offset)
        elif opcode == 99:
            self.__halt()
        else:
            raise ValueError(f"Invalid opcode: {opcode}, {self.registers}")


class Color(Enum):

    black = 0
    white = 1


class Direction(Enum):

    UP = auto()
    DOWN = auto()
    LEFT = auto()
    RIGHT = auto()


class Robot:

    x = 0
    y = 0

    direction = Direction.UP
    stats = {
        Direction.UP: 0,
        Direction.DOWN: 0,
        Direction.LEFT: 0,
        Direction.RIGHT: 0
    }

    def move(self, instruction: int):
        """Instruction 0 for turn left and 1 for turn right """

        if self.direction == Direction.UP:
            if instruction:
                self.x += 1
                new_direction = Direction.RIGHT
            else:
                self.x -= 1
                new_direction = Direction.LEFT
        elif self.direction == Direction.DOWN:
            if instruction:
                self.x -= 1
                new_direction = Direction.LEFT
            else:
                self.x += 1
                new_direction = Direction.RIGHT
        elif self.direction == Direction.LEFT:
            if instruction:
                self.y += 1
                new_direction = Direction.UP
            else:
                self.y -= 1
                new_direction = Direction.DOWN
        elif self.direction == Direction.RIGHT:
            if instruction:
                self.y -= 1
                new_direction = Direction.DOWN
            else:
                self.y += 1
                new_direction = Direction.UP
        else:
            raise ValueError(f"Unknown direction: {self.direction}")

        self.stats[new_direction] += 1
        self.direction = new_direction


Panel = namedtuple('Panel', 'x y color')


class Spaceship:

    def __init__(self):
        self.panels = list()

    def __contains__(self, panel: Tuple[int, int]) -> bool:
        for _panel in self.panels:
            if _panel.x == panel[0] and _panel.y == panel[1]:
                return True
        return False

    def __iter__(self):
        for panel in self.panels:
            yield panel

    def add(self, panel: Panel) -> None:
        if (panel.x, panel.y) in self:
            tmp_panel = self.get(panel.x, panel.y)
            self.panels.remove(tmp_panel)
        self.panels.append(panel)

    def get(self, x: int, y: int) -> Panel:
        """Return the panel at the given position."""
        for _panel in self.panels:
            if _panel.x == x and _panel.y == y:
                return _panel
        raise ValueError(f"Cannt find panel at position: {x}:{y}")


def resolve():
    logging.info(f"Start {__name__}")

    program_data = open('resources/day11#part1-input.txt', 'r').read()

    robot = Robot()
    spaceship = Spaceship()
    color = Color.white

    program = Program(program_data, Queue(), Queue(), memory_size=4096)
    program.start()
    first_round = True

    while program.status != "halt":
        if not (robot.x, robot.y) in spaceship:
            color = Color.black if not first_round else Color.white
        else:
            panel = spaceship.get(robot.x, robot.y)
            color = panel.color
        program.inputs.put(color.value)
        spaceship.add(Panel(robot.x, robot.y, Color(program.outputs.get())))
        robot.move(program.outputs.get())
        first_round = False

    logging.info(f"Solution to day 11 #part 1 is:{len(spaceship.panels)}")

    logging.info(f"Solution to day 11 #part 2 is:")

    min_x, min_y, max_x, max_y = 0, 0, 0, 0

    for panel in spaceship:
        if panel.x < min_x:
            min_x = panel.x
        if panel.x > max_x:
            max_x = panel.x
        if panel.y < min_y:
            min_y = panel.y
        if panel.y > max_y:
            max_y = panel.y

    for x in range(min_x, max_x + 1):
        for y in range(min_y, max_y + 1):
            p = " "
            if (x, y) in spaceship:
                p = "1" if spaceship.get(x, y).color.value else " "

            print(p, end="")
        print()
