#!/usr/bin/env python3
# coding: utf-8

import os
import argparse
import logging
import requests
import importlib
import html2markdown
from bs4 import BeautifulSoup

level = logging.DEBUG if os.getenv("DEBUG", "") else logging.INFO

logging.basicConfig(
    format='%(asctime)s [%(levelname).10s] %(module).15s: %(message)s',
    datefmt='%H:%M:%S',
    level=level
)

ENCODING = """# coding: utf-8

import logging

"""

LF = "\n"
COMMENT = '"' * 3
LF_COMMENT = f"{LF}{COMMENT}{LF}"
RESOLV_SIGNATURE = """

def resolve():
    logging.info(f"Start {__name__}")

"""


def dump_challenge_skeleton(data: str, day: int):
    with open(f'day{day}-challenge.py', 'w') as f:
        f.write(ENCODING)
        f.write(LF_COMMENT)
        f.writelines(data)
        f.write(LF_COMMENT)
        f.write(RESOLV_SIGNATURE)


def download_challenge(day: int) -> None:
    response = requests.get(f"https://adventofcode.com/2019/day/{day}")
    content = BeautifulSoup(response.text, 'html.parser')
    article = content.find('article')
    paragraphe = list(article.children)
    sanitized = filter(lambda x: True if str(x) != "\n" else False, paragraphe)
    formated = html2markdown.convert("".join(map(lambda x: str(x), sanitized)))
    dump_challenge_skeleton(formated, day)


def download(args):
    logging.info(f"Download challenge: {args.day}")
    download_challenge(args.day)
    logging.info(f"Challenge {args.day} dumped in file day{args.day}-challenge.py")


def execute(args):
    logging.info(f"Execute challenge: {args.day}")

    try:
        challenge = importlib.import_module(f"day{args.day}-challenge")
        challenge.resolve()
    except ImportError:
        logging.error(f"ay be you did not download it?")
    except Exception as er:
        if os.getenv("DEBUG"):
            import traceback
            print(traceback.format_exc())
        logging.error(f"Error while solving challenge {args.day}: {er}")


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Adventofcode 2K19")
    subparsers = parser.add_subparsers(dest="command",
                                       help="Command to execute for adventofcode 2K19!")
    subparsers.required = True

    subparser_dl = subparsers.add_parser(name="download",
                                         help="download new challenge")
    subparser_dl.add_argument("day", help="Which day should we download?")
    subparser_dl.set_defaults(func=download)

    subparser_exec = subparsers.add_parser(name="execute",
                                           help="execute challenge")
    subparser_exec.add_argument("day", help="Which day challenge to execute")

    subparser_exec.set_defaults(func=execute)

    args = parser.parse_args()
    args.func(args)
