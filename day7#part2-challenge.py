# coding: utf-8

import os
import time
import logging
import itertools
from typing import Tuple, List
from threading import Thread
from queue import Queue

"""
# --- Day 7: Amplification Circuit ---

Based on the navigational maps, you're going to need to send more power to your ship's thrusters to reach Santa in time. To do this, you'll need to configure a series of [amplifiers](https://en.wikipedia.org/wiki/Amplifier) already installed on the ship.

There are five <span title="As you can see, I know exactly how rockets work.">amplifiers connected in series</span>; each one receives an input signal and produces an output signal. They are connected such that the first amplifier's output leads to the second amplifier's input, the second amplifier's output leads to the third amplifier's input, and so on. The first amplifier's input value is `` 0 ``, and the last amplifier's output leads to your ship's thrusters.

        O-------O  O-------O  O-------O  O-------O  O-------O
    0 -&gt;| Amp A |-&gt;| Amp B |-&gt;| Amp C |-&gt;| Amp D |-&gt;| Amp E |-&gt; (to thrusters)
        O-------O  O-------O  O-------O  O-------O  O-------O

The Elves have sent you some _Amplifier Controller Software_ (your puzzle input), a program that should run on your [existing Intcode computer](5). Each amplifier will need to run a copy of the program.

When a copy of the program starts running on an amplifier, it will first use an input instruction to ask the amplifier for its current _phase setting_ (an integer from `` 0 `` to `` 4 ``). Each phase setting is used _exactly once_, but the Elves can't remember which amplifier needs which phase setting.

The program will then call another input instruction to get the amplifier's input signal, compute the correct output signal, and supply it back to the amplifier with an output instruction. (If the amplifier has not yet received an input signal, it waits until one arrives.)

Your job is to _find the largest output signal that can be sent to the thrusters_ by trying every possible combination of phase settings on the amplifiers. Make sure that memory is not shared or reused between copies of the program.

For example, suppose you want to try the phase setting sequence `` 3,1,2,4,0 ``, which would mean setting amplifier `` A `` to phase setting `` 3 ``, amplifier `` B `` to setting `` 1 ``, `` C `` to `` 2 ``, `` D `` to `` 4 ``, and `` E `` to `` 0 ``. Then, you could determine the output signal that gets sent from amplifier `` E `` to the thrusters with the following steps:

*   Start the copy of the amplifier controller software that will run on amplifier `` A ``. At its first input instruction, provide it the amplifier's phase setting, `` 3 ``. At its second input instruction, provide it the input signal, `` 0 ``. After some calculations, it will use an output instruction to indicate the amplifier's output signal.
*   Start the software for amplifier `` B ``. Provide it the phase setting (`` 1 ``) and then whatever output signal was produced from amplifier `` A ``. It will then produce a new output signal destined for amplifier `` C ``.
*   Start the software for amplifier `` C ``, provide the phase setting (`` 2 ``) and the value from amplifier `` B ``, then collect its output signal.
*   Run amplifier `` D ``'s software, provide the phase setting (`` 4 ``) and input value, and collect its output signal.
*   Run amplifier `` E ``'s software, provide the phase setting (`` 0 ``) and input value, and collect its output signal.

The final output signal from amplifier `` E `` would be sent to the thrusters. However, this phase setting sequence may not have been the best one; another sequence might have sent a higher signal to the thrusters.

Here are some example programs:

*

    Max thruster signal _`` 43210 ``_ (from phase setting sequence `` 4,3,2,1,0 ``):



        3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0


*

    Max thruster signal _`` 54321 ``_ (from phase setting sequence `` 0,1,2,3,4 ``):



        3,23,3,24,1002,24,10,24,1002,23,-1,23,
        101,5,23,23,1,24,23,23,4,23,99,0,0


*

    Max thruster signal _`` 65210 ``_ (from phase setting sequence `` 1,0,4,3,2 ``):



        3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,
        1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0



Try every combination of phase settings on the amplifiers. _What is the highest signal that can be sent to the thrusters?_

--- Part Two ---
It's no good - in this configuration, the amplifiers can't generate a large enough output signal to produce the thrust you'll need. The Elves quickly talk you through rewiring the amplifiers into a feedback loop:

      O-------O  O-------O  O-------O  O-------O  O-------O
0 -+->| Amp A |->| Amp B |->| Amp C |->| Amp D |->| Amp E |-.
   |  O-------O  O-------O  O-------O  O-------O  O-------O |
   |                                                        |
   '--------------------------------------------------------+
                                                            |
                                                            v
                                                     (to thrusters)
Most of the amplifiers are connected as they were before; amplifier A's output is connected to amplifier B's input, and so on. However, the output from amplifier E is now connected into amplifier A's input. This creates the feedback loop: the signal will be sent through the amplifiers many times.

In feedback loop mode, the amplifiers need totally different phase settings: integers from 5 to 9, again each used exactly once. These settings will cause the Amplifier Controller Software to repeatedly take input and produce output many times before halting. Provide each amplifier its phase setting at its first input instruction; all further input/output instructions are for signals.

Don't restart the Amplifier Controller Software on any amplifier during this process. Each one should continue receiving and sending signals until it halts.

All signals sent or received in this process will be between pairs of amplifiers except the very first signal and the very last signal. To start the process, a 0 signal is sent to amplifier A's input exactly once.

Eventually, the software on the amplifiers will halt after they have processed the final loop. When this happens, the last output signal from amplifier E is sent to the thrusters. Your job is to find the largest output signal that can be sent to the thrusters using the new phase settings and feedback loop arrangement.

Here are some example programs:

Max thruster signal 139629729 (from phase setting sequence 9,8,7,6,5):

3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,
27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5
Max thruster signal 18216 (from phase setting sequence 9,7,8,5,6):

3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,
-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,
53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10
Try every combination of the new phase settings on the amplifier feedback loop. What is the highest signal that can be sent to the thrusters?
"""


def debug(func):
    """Debug utility function."""
    def wrapper(*args):
        if os.getenv("DEBUG"):
            logging.debug(f"Registers: {args[0].registers}")
            logging.debug(f"Program: {args[0].program[:20]}")
        return func(*args)
    return wrapper


class Register:

    PC = 0
    EIP = 0
    EBP = 0

    def __repr__(self):
        return f"PC: {self.PC}, EIP: {self.EIP}, EBP: {self.EBP}"


class Program(Thread):

    def __init__(self, data: str, inputs: Queue, outputs: Queue, memory_size: int = 1024):
        super(Program, self).__init__()
        self.data = data
        self.status = "idle"
        self.program = [int(instruction) for instruction in self.data.split(',')]
        self.inputs = inputs
        self.outputs = outputs
        self.memory_size = memory_size
        self.registers = Register()
        self.__alloc_memory()

    def __alloc_memory(self) -> None:
        """Expended program memory space for futher data allocation"""

        for _ in range(self.memory_size):
            self.program.append(0)

    def run(self) -> None:
        """Start program execution """

        logging.debug(f"Allocate additional memory of size: {self.memory_size}, program: {self.data}")
        logging.debug(f"Start program!")
        self.status = "running"

        while True:
            try:
                opcode, modes = self.__parse_current_instruction()
                self.__execute_opcode(opcode, modes)
            except StopIteration:
                break

        logging.debug(f"End program execution: output: {self.outputs.qsize()}")

    def __halt(self) -> Exception:
        self.status = "halt"
        raise StopIteration("Halt program: opcode: 99")

    def __load_r(self, mode: int, addr: int) -> int:
        """Load instruction parameters, for read parameters modes are:
        0: immediate => read value pointed at the address of the paremeter
        1: position => value of the parameter is the parameter
        2: relative => read value pointed at the address of the parameter + EBP"""

        if mode == 0:
            return self.program[self.program[addr]]
        elif mode == 1:
            return self.program[addr]
        elif mode == 2:
            return self.program[self.registers.EBP + self.program[addr]]
        else:
            raise ValueError(f"Unknown mode:{mode}!")

    def __load_w(self, mode: int, index: int) -> int:
        """Load instruction parameters, for write parameters, modes are:
        0: immediate => read value pointed at the address of the paremeter
        2: relative => read value pointed at the address of the parameter + EBP"""

        if mode == 0:
            return self.program[index]
        elif mode == 2:
            return self.program[index] + self.registers.EBP
        else:
            raise ValueError(f"Unknown mode:{mode}!")

    def __store(self, value: int, x_addr: int) -> None:
        """Store value at the given address of the program."""

        self.program[x_addr] = value

    def __add(self, x: int, y: int, dest: int) -> None:
        """opcode: `01`, PC: `4`, add x and y and store the value in dest."""
        self.__store(x + y, dest)
        self.registers.EIP += 4

    def __mul(self, x: int, y: int, dest: int) -> None:
        """opcode: `02`, PC: `4`, multiply x and y and store the value in dest."""

        self.__store(x * y, dest)
        self.registers.EIP += 4

    def stdin(self, dest: int) -> None:
        """opcode: `03`, PC: `2`, get stdin and store it to dest."""

        self.__store(int(self.inputs.get()), dest)
        self.registers.EIP += 2

    def stdout(self, value: int) -> None:
        """opcode: `04`, PC: `2`, stdout value."""

        self.outputs.put(value)
        self.registers.EIP += 2
        logging.debug(f">&1 {value}")

    def __jz(self, x: int, addr: int) -> None:
        """opcode: `05`, PC: `3`, jump to addr if x is non-zero else it does nothing."""

        if x:
            self.registers.EIP = addr
        else:
            self.registers.EIP += 3

    def __jnz(self, x: int, addr: int) -> None:
        """opcode: `06`, PC: `3`, jump to addr if x is zero else it does nothing."""

        if not x:
            self.registers.EIP = addr
        else:
            self.registers.EIP += 3

    def __lt(self, x: int, y: int, dest: int) -> None:
        """opcode: `07`, PC: `4`, Compare if x lower then y write `1` to dest else `0`."""

        if x < y:
            self.__store(1, dest)
        else:
            self.__store(0, dest)
        self.registers.EIP += 4

    def __eq(self, x: int, y: int, dest: int) -> None:
        """opcode: `08`, PC: `4` if x equal to y then write `1` to dest else `0`."""

        if x == y:
            self.__store(1, dest)
        else:
            self.__store(0, dest)

        self.registers.EIP += 4

    def __sb(self, offset: int) -> None:
        """opcode: `09`, Move register `EBP` by the value of offset (could be negative)."""

        self.registers.EBP += offset
        self.registers.EIP += 2

    def __parse_current_instruction(self) -> Tuple[int, List[int]]:
        """Parse current instruction."""

        instruction = self.program[self.registers.EIP]
        opcode = instruction % 100
        modes = [
            instruction // 100 % 10,
            instruction // 1000 % 10,
            instruction // 10000 % 10
        ]

        return opcode, modes

    @debug
    def __execute_opcode(self, opcode: int, modes: list) -> None:
        """Execute opcode """

        logging.debug(f"Going to execute opcode: {opcode}")

        if opcode == 1:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            y = self.__load_r(modes[1], self.registers.EIP + 2)
            dest = self.__load_w(modes[2], self.registers.EIP + 3)
            self.__add(x, y, dest)
        elif opcode == 2:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            y = self.__load_r(modes[1], self.registers.EIP + 2)
            dest = self.__load_w(modes[2], self.registers.EIP + 3)
            self.__mul(x, y, dest)
        elif opcode == 3:
            dest = self.__load_w(modes[0], self.registers.EIP + 1)
            self.stdin(dest)
        elif opcode == 4:
            value = self.__load_r(modes[0], self.registers.EIP + 1)
            self.stdout(value)
        elif opcode == 5:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            addr = self.__load_r(modes[1], self.registers.EIP + 2)
            self.__jz(x, addr)
        elif opcode == 6:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            addr = self.__load_r(modes[1], self.registers.EIP + 2)
            self.__jnz(x, addr)
        elif opcode == 7:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            y = self.__load_r(modes[1], self.registers.EIP + 2)
            dest = self.__load_w(modes[2], self.registers.EIP + 3)
            self.__lt(x, y, dest)
        elif opcode == 8:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            y = self.__load_r(modes[1], self.registers.EIP + 2)
            dest = self.__load_w(modes[2], self.registers.EIP + 3)
            self.__eq(x, y, dest)
        elif opcode == 9:
            offset = self.__load_r(modes[0], self.registers.EIP + 1)
            self.__sb(offset)
        elif opcode == 99:
            self.__halt()
        else:
            raise ValueError(f"Invalid opcode: {opcode}, {self.registers}")


def amplifiers_signal_feedback(program: str) -> tuple:

    outputs = set()

    for signals in itertools.permutations(range(5, 10)):

        a_out, b_out, c_out, d_out, e_out = Queue(), Queue(), Queue(), Queue(), Queue()
        a_out.put(signals[1])
        b_out.put(signals[2])
        c_out.put(signals[3])
        d_out.put(signals[4])
        e_out.put(signals[0])
        e_out.put(0)

        a = Program(data=program, inputs=e_out, outputs=a_out)
        b = Program(data=program, inputs=a_out, outputs=b_out)
        c = Program(data=program, inputs=b_out, outputs=c_out)
        d = Program(data=program, inputs=c_out, outputs=d_out)
        e = Program(data=program, inputs=d_out, outputs=e_out)

        try:
            a.start()
            b.start()
            c.start()
            d.start()
            e.start()
            while e.status != "halt":
                time.sleep(.1)
            outputs.add(e.outputs.get())
        except Exception as er:
            logging.error(f"Unknown error: {er}")

    return max(outputs)


def resolve():
    logging.info("Start")
    program = open('resources/day7-input.txt', 'r').read()
    logging.info(f"Solution of day 7 part #2: {amplifiers_signal_feedback(program)}")
