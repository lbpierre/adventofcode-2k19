# coding: utf-8

import logging


"""
## --- Day 4: Secure Container ---

You arrive at the Venus fuel depot only to discover it's protected by a password. The Elves had written the password on a sticky note, but someone <span title="Look on the bright side - isn't it more secure if nobody knows the password?">threw it out</span>.

However, they do remember a few key facts about the password:

*   It is a six-digit number.
*   The value is within the range given in your puzzle input.
*   Two adjacent digits are the same (like `` 22 `` in <code>1<em>22</em>345</code>).
*   Going from left to right, the digits _never decrease_; they only ever increase or stay the same (like `` 111123 `` or `` 135679 ``).

Other than the range rule, the following are true:

*   `` 111111 `` meets these criteria (double `` 11 ``, never decreases).
*   <code>2234<em>50</em></code> does not meet these criteria (decreasing pair of digits `` 50 ``).
*   `` 123789 `` does not meet these criteria (no double).

_How many different passwords_ within the range given in your puzzle input meet these criteria?

Your puzzle answer was 1864.

The first half of this puzzle is complete! It provides one gold star: *

## --- Part Two ---
An Elf just remembered one more important detail: the two adjacent matching digits are not part of a larger group of matching digits.

Given this additional criterion, but still ignoring the range rule, the following are now true:

112233 meets these criteria because the digits never decrease and all repeated digits are exactly two digits long.
123444 no longer meets the criteria (the repeated 44 is part of a larger group of 444).
111122 meets the criteria (even though 1 is repeated more than twice, it still contains a double 22).
How many different passwords within the range given in your puzzle input meet all of the criteria?

Your puzzle input is still 137683-596253.
"""

ordered = lambda x: ''.join(sorted(x)) == x
adjacent_digits = lambda password: list(filter(lambda x: x > 1 and x <= 2, map(lambda x: password.count(x), password)))


def gen_password(first, last):
    passwords = set()

    for p in map(str, range(first, last)):
        if adjacent_digits(p):
            if ordered(p):
                passwords.add(p)

    return len(passwords)


def resolve():
    logging.info("Start")

    logging.info(f"Solution to day 4 part 2: {gen_password(137683,596253)}")
