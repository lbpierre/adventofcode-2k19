# coding: utf-8

import sys
import logging

from math import atan2
from math import hypot
from math import pi
from math import degrees
from collections import namedtuple
from typing import Tuple, Set
from termcolor import colored

"""
# --- Day 10: Monitoring Station ---

You fly into the asteroid belt and reach the Ceres monitoring station. The Elves here have an emergency: they're having trouble tracking all of the asteroids and can't be sure they're safe.

The Elves would like to build a new monitoring station in a nearby area of space; they hand you a map of all of the asteroids in that region (your puzzle input).

The map indicates whether each position is empty (`` . ``) or contains an asteroid (`` # ``). The asteroids are much smaller than they appear on the map, and every asteroid is exactly in the center of its marked position. The asteroids can be described with `` X,Y `` coordinates where `` X `` is the distance from the left edge and `` Y `` is the distance from the top edge (so the top-left corner is `` 0,0 `` and the position immediately to its right is `` 1,0 ``).

Your job is to figure out which asteroid would be the best place to build a _new monitoring station_. A monitoring station can _detect_ any asteroid to which it has _direct line of sight_ - that is, there cannot be another asteroid _exactly_ between them. This line of sight can be at any angle, not just lines aligned to the grid or <span title="The Elves on Ceres are clearly not concerned with honor.">diagonally</span>. The _best_ location is the asteroid that can _detect_ the largest number of other asteroids.

For example, consider the following map:

<pre><code>.#..#
.....
#####
....#
...<em>#</em>#
</code></pre>

The best location for a new monitoring station on this map is the highlighted asteroid at `` 3,4 `` because it can detect `` 8 `` asteroids, more than any other location. (The only asteroid it cannot detect is the one at `` 1,0 ``; its view of this asteroid is blocked by the asteroid at `` 2,2 ``.) All other asteroids are worse locations; they can detect `` 7 `` or fewer other asteroids. Here is the number of other asteroids a monitoring station on each asteroid could detect:

    .7..7
    .....
    67775
    ....7
    ...87

Here is an asteroid (`` # ``) and some examples of the ways its line of sight might be blocked. If there were another asteroid at the location of a capital letter, the locations marked with the corresponding lowercase letter would be blocked and could not be detected:

    # .........
    ...A......
    ...B..a...
    .EDCG....a
    ..F.c.b...
    .....c....
    ..efd.c.gb
    .......c..
    ....f...c.
    ...e..d..c

Here are some larger examples:

*

    Best is `` 5,8 `` with `` 33 `` other asteroids detected:



    <pre><code>......#.#.
# ..#.#....
..#######.
.#.#.###..
.#..#.....
..#....#.#
# ..#....#.
.##.#..###
# ...<em>#</em>..#.
.#....####
</code></pre>


*

    Best is `` 1,2 `` with `` 35 `` other asteroids detected:



    <pre><code>#.#...#.#.
.###....#.
.<em>#</em>....#...
##.#.#.#.#
....#.#.#.
.##..###.#
..#...##..
..##....##
......#...
.####.###.
</code></pre>


*

    Best is `` 6,3 `` with `` 41 `` other asteroids detected:



    <pre><code>.#..#..###
####.###.#
....###.#.
..###.<em>#</em>#.#
# .##.#.#.
....###..#
..#.#..#.#
#..#.#.###
.##...##.#
.....#.#..
</code></pre>


*

    Best is `` 11,13 `` with `` 210 `` other asteroids detected:



    <pre><code>.#..##.###...#######
# .############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
# .##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
# ...#.####<em>#</em>#####...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##
</code></pre>



Find the best location for a new monitoring station. _How many other asteroids can be detected from that location?_


# --- Part Two ---
Once you give them the coordinates, the Elves quickly deploy an Instant Monitoring Station to the location and discover the worst: there are simply too many asteroids.

The only solution is complete vaporization by giant laser.

Fortunately, in addition to an asteroid scanner, the new monitoring station also comes equipped with a giant rotating laser perfect for vaporizing asteroids. The laser starts by pointing up and always rotates clockwise, vaporizing any asteroid it hits.

If multiple asteroids are exactly in line with the station, the laser only has enough power to vaporize one of them before continuing its rotation. In other words, the same asteroids that can be detected can be vaporized, but if vaporizing one asteroid makes another one detectable, the newly-detected asteroid won't be vaporized until the laser has returned to the same position by rotating a full 360 degrees.

For example, consider the following map, where the asteroid with the new monitoring station (and laser) is marked X:

.#....#####...#..
##...##.#####..##
# ...#...#.#####.
..#.....X...###..
..#.#.....#....##
The first nine asteroids to get vaporized, in order, would be:

.#....###24...#..
##...##.13#67..9#
# ...#...5.8####.
..#.....X...###..
..#.#.....#....##
Note that some asteroids (the ones behind the asteroids marked 1, 5, and 7) won't have a chance to be vaporized until the next full rotation. The laser continues rotating; the next nine to be vaporized are:

.#....###.....#..
##...##...#.....#
# ...#......1234.
..#.....X...5##..
..#.9.....8....76
The next nine to be vaporized are then:

.8....###.....#..
56...9#...#.....#
34...7...........
..2.....X....##..
..1..............
Finally, the laser completes its first full rotation (1 through 3), a second rotation (4 through 8), and vaporizes the last asteroid (9) partway through its third rotation:

......234.....6..
......1...5.....7
.................
........X....89..
.................
In the large example above (the one with the best monitoring station location at 11,13):

The 1st asteroid to be vaporized is at 11,12.
The 2nd asteroid to be vaporized is at 12,1.
The 3rd asteroid to be vaporized is at 12,2.
The 10th asteroid to be vaporized is at 12,8.
The 20th asteroid to be vaporized is at 16,0.
The 50th asteroid to be vaporized is at 16,9.
The 100th asteroid to be vaporized is at 10,16.
The 199th asteroid to be vaporized is at 9,6.
The 200th asteroid to be vaporized is at 8,2.
The 201st asteroid to be vaporized is at 10,9.
The 299th and final asteroid to be vaporized is at 11,1.
The Elves are placing bets on which will be the 200th asteroid to be vaporized. Win the bet by determining which asteroid that will be; what do you get if you multiply its X coordinate by 100 and then add its Y coordinate? (For example, 8,2 becomes 802.)--- Part Two ---
Once you give them the coordinates, the Elves quickly deploy an Instant Monitoring Station to the location and discover the worst: there are simply too many asteroids.

The only solution is complete vaporization by giant laser.

Fortunately, in addition to an asteroid scanner, the new monitoring station also comes equipped with a giant rotating laser perfect for vaporizing asteroids. The laser starts by pointing up and always rotates clockwise, vaporizing any asteroid it hits.

If multiple asteroids are exactly in line with the station, the laser only has enough power to vaporize one of them before continuing its rotation. In other words, the same asteroids that can be detected can be vaporized, but if vaporizing one asteroid makes another one detectable, the newly-detected asteroid won't be vaporized until the laser has returned to the same position by rotating a full 360 degrees.

For example, consider the following map, where the asteroid with the new monitoring station (and laser) is marked X:

.#....#####...#..
##...##.#####..##
# ...#...#.#####.
..#.....X...###..
..#.#.....#....##
The first nine asteroids to get vaporized, in order, would be:

.#....###24...#..
##...##.13#67..9#
# ...#...5.8####.
..#.....X...###..
..#.#.....#....##
Note that some asteroids (the ones behind the asteroids marked 1, 5, and 7) won't have a chance to be vaporized until the next full rotation. The laser continues rotating; the next nine to be vaporized are:

.#....###.....#..
##...##...#.....#
# ...#......1234.
..#.....X...5##..
..#.9.....8....76
The next nine to be vaporized are then:

.8....###.....#..
56...9#...#.....#
34...7...........
..2.....X....##..
..1..............
Finally, the laser completes its first full rotation (1 through 3), a second rotation (4 through 8), and vaporizes the last asteroid (9) partway through its third rotation:

......234.....6..
......1...5.....7
.................
........X....89..
.................
In the large example above (the one with the best monitoring station location at 11,13):

The 1st asteroid to be vaporized is at 11,12.
The 2nd asteroid to be vaporized is at 12,1.
The 3rd asteroid to be vaporized is at 12,2.
The 10th asteroid to be vaporized is at 12,8.
The 20th asteroid to be vaporized is at 16,0.
The 50th asteroid to be vaporized is at 16,9.
The 100th asteroid to be vaporized is at 10,16.
The 199th asteroid to be vaporized is at 9,6.
The 200th asteroid to be vaporized is at 8,2.
The 201st asteroid to be vaporized is at 10,9.
The 299th and final asteroid to be vaporized is at 11,1.

The Elves are placing bets on which will be the 200th asteroid to be vaporized. Win the bet by determining which asteroid that will be; what do you get if you multiply its X coordinate by 100 and then add its Y coordinate? (For example, 8,2 becomes 802.)
"""

ims = None

Vector = namedtuple('Vector', 'x y')

refvector = Vector(0, -1)


class Asteroid:

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.distance = 0
        self.angle = 0.0

    def __repr__(self):
        return f"{self.x}:{self.y}, angle:{self.angle}, distance:{self.distance}"


def angle(a1: Asteroid, a2: Asteroid) -> float: return atan2(a2.x - a1.x, a1.y - a2.y) % (2 * pi)


def clockwiseangle_and_distance(asteroid: Asteroid):

    global ims
    vector = Vector(asteroid.x - ims.x, asteroid.y - ims.y)

    lenvector = hypot(vector.x, vector.y)

    if lenvector == 0:
        return -pi, 0

    normalized = Vector(vector.x / lenvector, vector.y / lenvector)
    dotprod = normalized.x * refvector.x + normalized.y * refvector.y     # x1*x2 + y1*y2
    diffprod = refvector.y * normalized.x - refvector.x * normalized.y     # x1*y2 - y1*x2
    angle = atan2(diffprod, dotprod)
    # Negative angles represent counter-clockwise angles so we need to subtract them
    # from 2*pi (360 degrees)
    if angle < 0:
        return 2 * pi + angle, lenvector
    # I return first the angle because that's the primary sorting criterium
    # but if two vectors have the same angle then the shorter distance should come first.
    return angle, lenvector


def find_ims(space_map: str) -> Tuple[Set[Asteroid], Asteroid]:

    asteroids = list()

    for y, line in enumerate(space_map.split("\n")):
        for x, dot in enumerate(list(line)):
            if dot == "#":
                asteroids.append(Asteroid(x, y))

    sight = 0
    ims = None

    cpt = 0
    for current_asteroid in asteroids:
        angles = set()
        for test_asteroid in asteroids:
            if test_asteroid == current_asteroid:
                continue

            ang = angle(current_asteroid, test_asteroid)
            angles.add(ang)
            if len(angles) >= sight:
                sight = len(angles)
                ims = current_asteroid

    print(f"{sight}, {current_asteroid}")
    logging.info(f"IMS position: {ims.x},{ims.y}")
    logging.info(f"Solution day10 part #1 is: {sight}")

    return asteroids, ims


def colored_asteroid(space_map: str, asteroid: Asteroid, color="red") -> None:
    global ims
    tmp_space_map = list(map(list, space_map.split('\n')))
    tmp_space_map[ims.y][ims.x] = colored("#", "white", f"on_red", attrs=["bold"])
    tmp_space_map[asteroid.y][asteroid.x] = colored("#", "white", f"on_{color}", attrs=["bold"])
    tmp_space_map = "\n".join(["".join(line) for line in tmp_space_map])
    print(tmp_space_map, end="\r")
    space_map = list(map(list, space_map.split('\n')))
    space_map[asteroid.y][asteroid.x] = "."
    space_map = "\n".join(["".join(line) for line in space_map])
    return space_map


def distance(a1: Asteroid, a2: Asteroid) -> int: return (abs(a1.x - a2.x) + abs(a1.y - a2.y))


def resolve():
    logging.info(f"Start {__name__}")
    space_map = open('resources/day10-input.txt', 'r').read()

    global ims
    asteroids, ims = find_ims(space_map)
    asteroids.remove(ims)
    logging.info(f"IMS: ({ims.x},{ims.y})")

    def win_func(asteroid: Asteroid) -> int: return (asteroid.x * 100 + asteroid.y)

    sorted_asteroids = dict()
    for asteroid in asteroids:
        ang, dist = clockwiseangle_and_distance(asteroid)
        asteroid.angle = ang
        asteroid.distance = dist
        if not ang in sorted_asteroids.keys():
            sorted_asteroids[ang] = []
        sorted_asteroids[ang].append(asteroid)

    ss = []
    for i in sorted(sorted_asteroids.keys()):
        sorted_asteroids[i].sort(key=lambda x: x.distance)
        ss.append(sorted_asteroids[i])

    ss = list(reversed(ss))
    if ss[-1][0].angle == 0.0:
        tmp = ss.pop(-1)
        ss.insert(0, tmp)

    cpt = 1
    import time
    while ss:
        for i, _asteroids in enumerate(ss):
            if len(_asteroids) > 0:
                ast = _asteroids.pop(0)

                time.sleep(0.01)
                if cpt == 202:
                    space_map = colored_asteroid(space_map, ast, "green")
                    print(ast)
                    return logging.info(f"Solution of day 10 part #2: {win_func(ast)}")
                else:
                    space_map = colored_asteroid(space_map, ast, "blue")
                cpt += 1
                [sys.stdout.write("\033[F") for _ in range(len(space_map.split('\n')) - 1)]
            else:
                del ss[i]
