# coding: utf-8

import logging


"""
# --- Day 5: Sunny with a Chance of Asteroids ---

You're starting to sweat as the ship makes its way toward Mercury. The Elves suggest that you get the air conditioner working by upgrading your ship computer to support the Thermal Environment Supervision Terminal.

The Thermal Environment Supervision Terminal (TEST) starts by running a _diagnostic program_ (your puzzle input). The TEST diagnostic program will run on [your existing Intcode computer](2) after a few modifications:

_First_, you'll need to add _two new instructions_:

*   Opcode `` 3 `` takes a single integer as _input_ and saves it to the position given by its only parameter. For example, the instruction `` 3,50 `` would take an input value and store it at address `` 50 ``.
*   Opcode `` 4 `` _outputs_ the value of its only parameter. For example, the instruction `` 4,50 `` would output the value at address `` 50 ``.

Programs that use these instructions will come with documentation that explains what should be connected to the input and output. The program `` 3,0,4,0,99 `` outputs whatever it gets as input, then halts.

_Second_, you'll need to add support for _parameter modes_:

Each parameter of an instruction is handled based on its parameter mode. Right now, your ship computer already understands parameter mode `` 0 ``, _position mode_, which causes the parameter to be interpreted as a _position_ - if the parameter is `` 50 ``, its value is _the value stored at address `` 50 `` in memory_. Until now, all parameters have been in position mode.

Now, your ship computer will also need to handle parameters in mode `` 1 ``, _immediate mode_. In immediate mode, a parameter is interpreted as a _value_ - if the parameter is `` 50 ``, its value is simply _`` 50 ``_.

Parameter modes are stored in the same value as the instruction's opcode. The opcode is a two-digit number based only on the ones and tens digit of the value, that is, the opcode is the rightmost two digits of the first value in an instruction. Parameter modes are single digits, one per parameter, read right-to-left from the opcode: the first parameter's mode is in the hundreds digit, the second parameter's mode is in the thousands digit, the third parameter's mode is in the ten-thousands digit, and so on. Any missing modes are `` 0 ``.

For example, consider the program `` 1002,4,3,4,33 ``.

The first instruction, `` 1002,4,3,4 ``, is a _multiply_ instruction - the rightmost two digits of the first value, `` 02 ``, indicate opcode `` 2 ``, multiplication. Then, going right to left, the parameter modes are `` 0 `` (hundreds digit), `` 1 `` (thousands digit), and `` 0 `` (ten-thousands digit, not present and therefore zero):

    ABCDE
     1002

    DE - two-digit opcode,      02 == opcode 2
     C - mode of 1st parameter,  0 == position mode
     B - mode of 2nd parameter,  1 == immediate mode
     A - mode of 3rd parameter,  0 == position mode,
                                      omitted due to being a leading zero

This instruction multiplies its first two parameters. The first parameter, `` 4 `` in position mode, works like it did before - its value is the value stored at address `` 4 `` (`` 33 ``). The second parameter, `` 3 `` in immediate mode, simply has value `` 3 ``. The result of this operation, `` 33 * 3 = 99 ``, is written according to the third parameter, `` 4 `` in position mode, which also works like it did before - `` 99 `` is written to address `` 4 ``.

Parameters that an instruction writes to will _never be in immediate mode_.

_Finally_, some notes:

*   It is important to remember that the instruction pointer should increase by _the number of values in the instruction_ after the instruction finishes. Because of the new instructions, this amount is no longer always `` 4 ``.
*   Integers can be negative: `` 1101,100,-1,4,0 `` is a valid program (find `` 100 + -1 ``, store the result in position `` 4 ``).

The TEST diagnostic program will start by requesting from the user the ID of the system to test by running an _input_ instruction - provide it `` 1 ``, the ID for the ship's air conditioner unit.

It will then perform a series of diagnostic tests confirming that various parts of the Intcode computer, like parameter modes, function correctly. For each test, it will run an _output_ instruction indicating how far the result of the test was from the expected value, where `` 0 `` means the test was successful. Non-zero outputs mean that a function is not working correctly; check the instructions that were run before the output instruction to see which one failed.

Finally, the program will output a _diagnostic code_ and immediately halt. This final output isn't an error; an output followed immediately by a halt means the program finished. If all outputs were zero except the diagnostic code, the diagnostic program ran successfully.

After providing `` 1 `` to the only input instruction and passing all the tests, _what diagnostic code does the program produce?_
"""


def execute_program(data: str) -> str:

    # Size programm instructions
    logging.debug(f"Execute program: {data}")
    program = [int(i) for i in data.split(',')]

    # Init Registers
    pc = 0
    eip = 0

    def exitProgram(info: str) -> Exception: raise StopIteration(info)
    def store(value: int, x_addr: int) -> None: program[x_addr] = value
    load = lambda mode, addr: program[program[addr]] if not mode else program[addr]
    setpc = lambda _opcode: _opcode + 1

    add = lambda x, y, dest: store(x + y, dest)  # 1 addition
    mul = lambda x, y, dest: store(x * y, dest)  # 2 multiplication
    stdin = lambda dest: store(int(input(">")), dest)  # 3 input
    stdout = lambda addr: print(load(1, addr))  # 4 output

    stop = lambda: exitProgram("OP code `exit` reach")  # 99

    # return opcode 01, 02, 03, 03 and modes as a list, eg: [0, 1, 0]
    opcode = lambda x: (int(str(x)[-2:]), [int(y) for y in str(x)[:-2]])

    # operators is the list of operator with their given instruction size
    operators = [(None, 0), (add, 4), (mul, 4), (stdin, 2), (stdout, 2)]

    # Nasty Instruction set completion
    fill = lambda x: operators.append((None, 0))
    list(map(fill, range(99 - len(operators))))
    operators.append((stop, -1))

    def execute_opcode(x: int, *args: list) -> int:

        operators[x][0](*args)

        # return the program counter for the next instruction
        return operators[x][1]

    def popargs(eip: int, _opcode: int, modes: list) -> list:
        if _opcode != 99:
            nb_args = operators[_opcode][1] - 1
            modes.reverse()
            if len(modes) != nb_args:
                [modes.append(0) for i in range(0, nb_args - len(modes))]

            args = []
            for mode in range(len(modes) - 1):
                args.append(load(modes[mode], eip + mode + 1))
            args.append(load(1, eip + len(modes)))
            return args
        return []

    for _ in range(0, len(program)):
        try:
            instruction = program[eip]
            _opcode, modes = opcode(instruction)
            logging.debug(f"{_opcode}: {modes}")
            args = popargs(eip, _opcode, modes)
            logging.debug(f"{_opcode}: ({args})")
            # execute the operator
            # and
            # increment the next instruction to execute by value
            # of the program counter
            eip += execute_opcode(_opcode, *args)
        except StopIteration:
            break

    output = ",".join([str(index) for index in program])
    return output


def resolve():
    logging.info(f"Start {__name__}")
    assert execute_program("1002,4,3,4,33") == "1002,4,3,4,99"
    assert execute_program("1101,100,-1,4,0") == "1101,100,-1,4,99"
    assert execute_program("3,0,4,0,99") == "123,0,4,0,99"

    program = open('resources/day5-input.txt', 'r').read()
    output = execute_program(program)
    logging.info(f"Day 5 solution: {output}")
