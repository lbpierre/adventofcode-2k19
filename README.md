# adventofcode-2K19

Adventofcode 2K19 challenges.

## Installation

`pip3 install -r requirements.txt`

## Usage

Download new challenge:

`./adventofcode.py download <day>` 


Execute a challenge:

`./adventofcode execute <day>`
