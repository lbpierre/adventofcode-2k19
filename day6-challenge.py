# coding: utf-8

import logging


"""
## --- Day 6: Universal Orbit Map ---

You've landed at the Universal Orbit Map facility on Mercury. Because navigation in space often involves transferring between orbits, the orbit maps here are useful for finding efficient routes between, for example, you and Santa. You download a map of the local orbits (your puzzle input).

Except for the universal Center of Mass (`` COM ``), every object in space is in orbit around <span title="What do you mean, Kerbal Space Program doesn't have accurate orbital physics?">exactly one other object</span>. An [orbit](https://en.wikipedia.org/wiki/Orbit) looks roughly like this:

                      \
                       \
                        |
                        |
    AAA--&gt; o            o &lt;--BBB
                        |
                        |
                       /
                      /

In this diagram, the object `` BBB `` is in orbit around `` AAA ``. The path that `` BBB `` takes around `` AAA `` (drawn with lines) is only partly shown. In the map data, this orbital relationship is written `` AAA)BBB ``, which means "`` BBB `` is in orbit around `` AAA ``".

Before you use your map data to plot a course, you need to make sure it wasn't corrupted during the download. To verify maps, the Universal Orbit Map facility uses _orbit count checksums_ - the total number of _direct orbits_ (like the one shown above) and _indirect orbits_.

Whenever `` A `` orbits `` B `` and `` B `` orbits `` C ``, then `` A `` _indirectly orbits_ `` C ``. This chain can be any number of objects long: if `` A `` orbits `` B ``, `` B `` orbits `` C ``, and `` C `` orbits `` D ``, then `` A `` indirectly orbits `` D ``.

For example, suppose you have the following map:

    COM)B
    B)C
    C)D
    D)E
    E)F
    B)G
    G)H
    D)I
    E)J
    J)K
    K)L

Visually, the above map of orbits looks like this:

            G - H       J - K - L
           /           /
    COM - B - C - D - E - F
                   \
                    I

In this visual representation, when two objects are connected by a line, the one on the right directly orbits the one on the left.

Here, we can count the total number of orbits as follows:

*   `` D `` directly orbits `` C `` and indirectly orbits `` B `` and `` COM ``, a total of `` 3 `` orbits.
*   `` L `` directly orbits `` K `` and indirectly orbits `` J ``, `` E ``, `` D ``, `` C ``, `` B ``, and `` COM ``, a total of `` 7 `` orbits.
*   `` COM `` orbits nothing.

The total number of direct and indirect orbits in this example is <code><em>42</em></code>.

_What is the total number of direct and indirect orbits_ in your map data?
"""

info = lambda x: (x.split(')')[0], x.split(')')[1]) if x else (None, None)


def count_links(paths, node_len=1):
    visited = set()
    count = 0
    for path in paths:
        logging.debug(f"Test path: {path}")
        for subpath in range(0, len(path) + 1, node_len):
            logging.debug(f"Count subpass: {path[:subpath]}, step:{subpath}")
            if path[:subpath] not in visited:
                visited.add(path[:subpath])
                count += (len(path[:subpath]) / node_len)
        logging.debug(f"counted links of {path} => {count}")

    return count


def build_paths(nodes: dict, edges: set) -> set:

    for n1, n2 in edges:
        nodes[n1].append(n2)

    logging.debug(f"Direct nodes: {nodes}")
    start = "COM"

    def visit_edges(node, visited, path, paths):

        if not nodes[node]:
            paths.add(path)

        for _node in nodes[node]:
            tmp = path + _node

            if tmp not in visited:
                logging.debug(f"add node: {_node} to path: {path}")
                visited.add(tmp)
                visit_edges(_node, visited, tmp, paths)

        return paths

    paths = visit_edges(start, set(), "", set())

    logging.debug(f" {paths}")
    return paths


def extract_nodes(raw_graph: str) -> tuple:

    nodes = dict()
    edges = set()

    for n1, n2 in map(info, raw_graph):
        if not n1:
            continue
        nodes[n1] = []
        nodes[n2] = []
        edges.add((n1, n2))
        node_len = len(n1)

    logging.debug(f"nodes: {nodes}")
    logging.debug(f"edges: {edges}")

    logging.info(f"node len: {node_len}")
    return (nodes, edges, node_len)


def compute(graph):
    nodes, edges, node_len = extract_nodes(graph)
    paths = build_paths(nodes, edges)
    nb_links = count_links(paths, node_len)
    return nb_links


def resolve():
    logging.info(f"Start {__name__}")

    path = """
 
            G - H       J - K - L
           /           /
    COM - B - C - D - E - F
                   \\
                    I
"""

    logging.debug(path)

    graph = """COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L"""

    solution = compute(graph.split('\n'))
    logging.info(f"Test solution is :{solution}")

    graph = """ABC)DEF
DEF)GHI
DEF)JKL
JKL)MNO
COM)ABC"""

    solution = compute(graph.split('\n'))
    logging.info(f"Test solution is :{solution}")

    graph = open('resources/day6-input.txt', 'r').read()
    solution = compute(graph.split('\n'))
    logging.info(f"Solution of day 6: {solution}")


if __name__ == "__main__":
    logging.basicConfig(
        format='%(asctime)s [%(levelname).10s] %(module).15s: %(message)s',
        datefmt='%H:%M:%S',
        level=logging.INFO
    )

    resolve()
