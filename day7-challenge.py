# coding: utf-8

import logging
import itertools

"""
# --- Day 7: Amplification Circuit ---

Based on the navigational maps, you're going to need to send more power to your ship's thrusters to reach Santa in time. To do this, you'll need to configure a series of [amplifiers](https://en.wikipedia.org/wiki/Amplifier) already installed on the ship.

There are five <span title="As you can see, I know exactly how rockets work.">amplifiers connected in series</span>; each one receives an input signal and produces an output signal. They are connected such that the first amplifier's output leads to the second amplifier's input, the second amplifier's output leads to the third amplifier's input, and so on. The first amplifier's input value is `` 0 ``, and the last amplifier's output leads to your ship's thrusters.

        O-------O  O-------O  O-------O  O-------O  O-------O
    0 -&gt;| Amp A |-&gt;| Amp B |-&gt;| Amp C |-&gt;| Amp D |-&gt;| Amp E |-&gt; (to thrusters)
        O-------O  O-------O  O-------O  O-------O  O-------O

The Elves have sent you some _Amplifier Controller Software_ (your puzzle input), a program that should run on your [existing Intcode computer](5). Each amplifier will need to run a copy of the program.

When a copy of the program starts running on an amplifier, it will first use an input instruction to ask the amplifier for its current _phase setting_ (an integer from `` 0 `` to `` 4 ``). Each phase setting is used _exactly once_, but the Elves can't remember which amplifier needs which phase setting.

The program will then call another input instruction to get the amplifier's input signal, compute the correct output signal, and supply it back to the amplifier with an output instruction. (If the amplifier has not yet received an input signal, it waits until one arrives.)

Your job is to _find the largest output signal that can be sent to the thrusters_ by trying every possible combination of phase settings on the amplifiers. Make sure that memory is not shared or reused between copies of the program.

For example, suppose you want to try the phase setting sequence `` 3,1,2,4,0 ``, which would mean setting amplifier `` A `` to phase setting `` 3 ``, amplifier `` B `` to setting `` 1 ``, `` C `` to `` 2 ``, `` D `` to `` 4 ``, and `` E `` to `` 0 ``. Then, you could determine the output signal that gets sent from amplifier `` E `` to the thrusters with the following steps:

*   Start the copy of the amplifier controller software that will run on amplifier `` A ``. At its first input instruction, provide it the amplifier's phase setting, `` 3 ``. At its second input instruction, provide it the input signal, `` 0 ``. After some calculations, it will use an output instruction to indicate the amplifier's output signal.
*   Start the software for amplifier `` B ``. Provide it the phase setting (`` 1 ``) and then whatever output signal was produced from amplifier `` A ``. It will then produce a new output signal destined for amplifier `` C ``.
*   Start the software for amplifier `` C ``, provide the phase setting (`` 2 ``) and the value from amplifier `` B ``, then collect its output signal.
*   Run amplifier `` D ``'s software, provide the phase setting (`` 4 ``) and input value, and collect its output signal.
*   Run amplifier `` E ``'s software, provide the phase setting (`` 0 ``) and input value, and collect its output signal.

The final output signal from amplifier `` E `` would be sent to the thrusters. However, this phase setting sequence may not have been the best one; another sequence might have sent a higher signal to the thrusters.

Here are some example programs:

*

    Max thruster signal _`` 43210 ``_ (from phase setting sequence `` 4,3,2,1,0 ``):



        3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0


*

    Max thruster signal _`` 54321 ``_ (from phase setting sequence `` 0,1,2,3,4 ``):



        3,23,3,24,1002,24,10,24,1002,23,-1,23,
        101,5,23,23,1,24,23,23,4,23,99,0,0


*

    Max thruster signal _`` 65210 ``_ (from phase setting sequence `` 1,0,4,3,2 ``):



        3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,
        1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0



Try every combination of phase settings on the amplifiers. _What is the highest signal that can be sent to the thrusters?_
"""


def execute_program(program: list, inputs: set) -> str:

    # Size programm instructions
    logging.debug(f"Execute program: {program}")

    inputs = iter(inputs)
    program_output = []

    # Init Registers
    pc = 0
    eip = 0

    def exitProgram(info: str) -> Exception: raise StopIteration(info)
    def store(value: int, x_addr: int) -> None: program[x_addr] = value
    # load = lambda mode, addr: program[program[addr]] if not mode else program[addr]

    load = lambda mode, addr: program[addr] if mode else program[program[addr]]
    add = lambda x, y, dest: store(x + y, dest)  # 1 addition
    mul = lambda x, y, dest: store(x * y, dest)  # 2 multiplication
    stdin = lambda dest: store(int(next(inputs)), dest)  # 3 input

    def stdout(value):
        # out = load(1, addr)
        logging.debug(value)  # 4 output
        program_output.append(value)

    jz = lambda x, addr: addr if x else (eip + 3)  # 5 jump-if-true
    jnz = lambda x, addr: addr if not x else (eip + 3)  # 6 jump-if-false
    jb = lambda x, y, dest: store(1, dest) if x < y else store(0, dest)  # 7 less than
    je = lambda x, y, dest: store(1, dest) if x == y else store(0, dest)  # 8 is equal

    pop_add_arg = lambda modes, eip: [load(modes[0], eip + 1), load(modes[1], eip + 2), load(1, eip + 3)]
    pop_mul_arg = lambda modes, eip: [load(modes[0], eip + 1), load(modes[1], eip + 2), load(1, eip + 3)]
    pop_stdin_arg = lambda modes, eip: [load(1, eip + 1)]
    pop_stdout_arg = lambda modes, eip: [load(modes[0], eip + 1)]
    pop_jz_arg = lambda modes, eip: [load(modes[0], eip + 1), load(modes[1], eip + 2)]
    pop_jnz_arg = lambda modes, eip: [load(modes[0], eip + 1), load(modes[1], eip + 2)]
    pop_jb_arg = lambda modes, eip: [load(modes[0], eip + 1), load(modes[1], eip + 2), load(1, eip + 3)]
    pop_je_arg = lambda modes, eip: [load(modes[0], eip + 1), load(modes[1], eip + 2), load(1, eip + 3)]

    nop = lambda: None
    stop = lambda: exitProgram("OP code `exit` reach")  # 99

    # return opcode 01, 02, 03, 03 and modes as a list, eg: [0, 1, 0]
    # opcode = lambda x: (int(str(x)[-2:]), [int(y) for y in str(x)[:-2]])
    opcode = lambda x: (int(str(x)[-2:]), str(x)[:-2])

    # operators is the list of operator with their given instruction size
    operators = [(nop, 1), (add, 4), (mul, 4), (stdin, 2), (stdout, 2),
                 (jz, 3), (jnz, 3), (jb, 4), (je, 4), (nop, 1)]

    # Nasty Instruction set completion
    fill = lambda x: operators.append((nop, 1))
    list(map(fill, range(99 - len(operators))))
    operators.append((stop, 0))

    def execute_opcode(x: int, *args: list) -> int:

        out = operators[x][0](*args)

        # return the program counter for the next instruction
        return (eip + operators[x][1]) if not out else out

    def popargs(eip: int, _opcode: int, modes: str) -> list:

        logging.debug("op: {}, eip: {}, modes: {}, program: {}".format(_opcode, eip, modes, program))
        nb_args = operators[_opcode][1] - 1
        _opcode = operators[_opcode][0]

        modes = [int(mode) for mode in modes.zfill(nb_args)[::-1]]
        args = []

        if _opcode == stop:
            args = []
        if _opcode == add:
            args = pop_add_arg(modes, eip)
        elif _opcode == mul:
            args = pop_mul_arg(modes, eip)
        elif _opcode == stdin:
            args = pop_stdin_arg(modes, eip)
        elif _opcode == stdout:
            args = pop_stdout_arg(modes, eip)
        elif _opcode == jz:
            args = pop_jz_arg(modes, eip)
        elif _opcode == jnz:
            args = pop_jnz_arg(modes, eip)
        elif _opcode == jb:
            args = pop_jb_arg(modes, eip)
        elif _opcode == je:
            args = pop_je_arg(modes, eip)

        return args

    for _ in range(0, len(program)):
        try:
            instruction = program[eip]
            _opcode, modes = opcode(instruction)
            args = popargs(eip, _opcode, modes)
            logging.debug(f"{_opcode}: ({args})")
            # execute the operator
            # and
            # increment the next instruction to execute by value
            # of the program counter
            eip = execute_opcode(_opcode, *args)
        except StopIteration:
            break
        logging.debug(f"{program}, EIP:{eip}")

    output = ",".join([str(index) for index in program])
    return (output, program_output)


def amplifiers_signal(program: str) -> str:

    best = 0
    program = [int(i) for i in program.split(',')]

    for signals in itertools.permutations(range(5)):
        output = [0]
        for setting in signals:
            inputs = [setting, output[0]]
            prog, output = execute_program(program.copy(), inputs)

            if int(output[0]) > best:
                best = int(output[0])
                settings = signals

    return signals, best


def resolve():
    logging.info("Start")
    prog = open('resources/day7-input.txt', 'r').read()
    print(amplifiers_signal(prog))
