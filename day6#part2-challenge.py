# coding: utf-8

import logging


"""
## --- Day 6: Universal Orbit Map ---

You've landed at the Universal Orbit Map facility on Mercury. Because navigation in space often involves transferring between orbits, the orbit maps here are useful for finding efficient routes between, for example, you and Santa. You download a map of the local orbits (your puzzle input).

Except for the universal Center of Mass (`` COM ``), every object in space is in orbit around <span title="What do you mean, Kerbal Space Program doesn't have accurate orbital physics?">exactly one other object</span>. An [orbit](https://en.wikipedia.org/wiki/Orbit) looks roughly like this:

                      \
                       \
                        |
                        |
    AAA--&gt; o            o &lt;--BBB
                        |
                        |
                       /
                      /

In this diagram, the object `` BBB `` is in orbit around `` AAA ``. The path that `` BBB `` takes around `` AAA `` (drawn with lines) is only partly shown. In the map data, this orbital relationship is written `` AAA)BBB ``, which means "`` BBB `` is in orbit around `` AAA ``".

Before you use your map data to plot a course, you need to make sure it wasn't corrupted during the download. To verify maps, the Universal Orbit Map facility uses _orbit count checksums_ - the total number of _direct orbits_ (like the one shown above) and _indirect orbits_.

Whenever `` A `` orbits `` B `` and `` B `` orbits `` C ``, then `` A `` _indirectly orbits_ `` C ``. This chain can be any number of objects long: if `` A `` orbits `` B ``, `` B `` orbits `` C ``, and `` C `` orbits `` D ``, then `` A `` indirectly orbits `` D ``.

For example, suppose you have the following map:

    COM)B
    B)C
    C)D
    D)E
    E)F
    B)G
    G)H
    D)I
    E)J
    J)K
    K)L

Visually, the above map of orbits looks like this:

            G - H       J - K - L
           /           /
    COM - B - C - D - E - F
                   \
                    I

In this visual representation, when two objects are connected by a line, the one on the right directly orbits the one on the left.

Here, we can count the total number of orbits as follows:

*   `` D `` directly orbits `` C `` and indirectly orbits `` B `` and `` COM ``, a total of `` 3 `` orbits.
*   `` L `` directly orbits `` K `` and indirectly orbits `` J ``, `` E ``, `` D ``, `` C ``, `` B ``, and `` COM ``, a total of `` 7 `` orbits.
*   `` COM `` orbits nothing.

The total number of direct and indirect orbits in this example is <code><em>42</em></code>.

_What is the total number of direct and indirect orbits_ in your map data?

--- Part Two ---

Now, you just need to figure out how many orbital transfers you (YOU) need to take to get to Santa (SAN).

You start at the object YOU are orbiting; your destination is the object SAN is orbiting. An orbital transfer lets you move from any object to an object orbiting or orbited by that object.

For example, suppose you have the following map:

COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN

Visually, the above map of orbits looks like this:

                          YOU
                         /
        G - H       J - K - L
       /           /
COM - B - C - D - E - F
               \
                I - SAN

In this example, YOU are in orbit around K, and SAN is in orbit around I. To move from K to I, a minimum of 4 orbital transfers are required:

    K to J
    J to E
    E to D
    D to I

Afterward, the map of orbits looks like this:

        G - H       J - K - L
       /           /
COM - B - C - D - E - F
               \
                I - SAN
                 \

                  YOU

What is the minimum number of orbital transfers required to move from the object YOU are orbiting to the object SAN is orbiting? (Between the objects they are orbiting - not between YOU and SAN.)

"""

info = lambda x: (x.split(')')[0], x.split(')')[1]) if x else (None, None)


def find_short_path(paths, node_len=1, start="YOU", end="END"):
    visited = set()
    _paths = []

    def parkour(node, path, visited):

        for neighbour in paths[node]:

            if neighbour not in visited:
                new_path = path.copy()
                visited.add(neighbour)
                new_path.add(neighbour)
                if neighbour == end:
                    _paths.append(path)
                parkour(neighbour, new_path, visited)

    # register start point and start parkouring the graph
    visited.add(start)
    parkour(start, set(), visited)

    return len(min(_paths)) - 1


def build_paths(nodes: dict, edges: set) -> set:

    for n1, n2 in edges:
        nodes[n1].add(n2)
        nodes[n2].add(n1)

    return nodes


def extract_nodes(raw_graph: str) -> tuple:

    nodes = dict()
    edges = set()

    for n1, n2 in map(info, raw_graph):
        if not n1:
            continue
        nodes[n1] = set()
        nodes[n2] = set()
        edges.add((n1, n2))
        edges.add((n2, n1))

    return (nodes, edges)


def compute(graph, start, end):
    nodes, edges = extract_nodes(graph)
    paths = build_paths(nodes, edges)
    path_length = find_short_path(paths, start=start, end=end)

    return path_length


def resolve():
    logging.info(f"Start {__name__}")

    graph = """COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN"""

    assert compute(graph.split('\n'), "YOU", "SAN") == 4

    graph = open('resources/day6-input.txt', 'r').read()
    solution = compute(graph.split('\n'), "YOU", "SAN")
    logging.info(f"Solution of day 6: {solution}")
