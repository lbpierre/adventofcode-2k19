# coding: utf-8

import os
import logging
from typing import Tuple, List

"""
# --- Day 9: Sensor Boost ---

You've just said goodbye to the rebooted rover and left Mars when you receive a faint distress signal coming from the asteroid belt. It must be the Ceres monitoring station!

In order to lock on to the signal, you'll need to boost your sensors. The Elves send up the latest _BOOST_ program - Basic Operation Of System Test.

While BOOST (your puzzle input) is capable of boosting your sensors, for <span title="Oh sure, NOW safety is a priority.">tenuous safety reasons</span>, it refuses to do so until the computer it runs on passes some checks to demonstrate it is a _complete Intcode computer_.

[Your existing Intcode computer](5) is missing one key feature: it needs support for parameters in _relative mode_.

Parameters in mode `` 2 ``, _relative mode_, behave very similarly to parameters in _position mode_: the parameter is interpreted as a position. Like position mode, parameters in relative mode can be read from or written to.

The important difference is that relative mode parameters don't count from address `` 0 ``. Instead, they count from a value called the _relative base_. The _relative base_ starts at `` 0 ``.

The address a relative mode parameter refers to is itself _plus_ the current _relative base_. When the relative base is `` 0 ``, relative mode parameters and position mode parameters with the same value refer to the same address.

For example, given a relative base of `` 50 ``, a relative mode parameter of `` -7 `` refers to memory address <code>50 + -7 = <em>43</em></code>.

The relative base is modified with the _relative base offset_ instruction:

*   Opcode `` 9 `` _adjusts the relative base_ by the value of its only parameter. The relative base increases (or decreases, if the value is negative) by the value of the parameter.

For example, if the relative base is `` 2000 ``, then after the instruction `` 109,19 ``, the relative base would be `` 2019 ``. If the next instruction were `` 204,-34 ``, then the value at address `` 1985 `` would be output.

Your Intcode computer will also need a few other capabilities:

*   The computer's available memory should be much larger than the initial program. Memory beyond the initial program starts with the value `` 0 `` and can be read or written like any other memory. (It is invalid to try to access memory at a negative address, though.)
*   The computer should have support for large numbers. Some instructions near the beginning of the BOOST program will verify this capability.

Here are some example programs that use these features:

*   `` 109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99 `` takes no input and produces a [copy of itself](https://en.wikipedia.org/wiki/Quine_(computing)) as output.
*   `` 1102,34915192,34915192,7,4,7,99,0 `` should output a 16-digit number.
*   `` 104,1125899906842624,99 `` should output the large number in the middle.

The BOOST program will ask for a single input; run it in test mode by providing it the value `` 1 ``. It will perform a series of checks on each opcode, output any opcodes (and the associated parameter modes) that seem to be functioning incorrectly, and finally output a BOOST keycode.

Once your Intcode computer is fully functional, the BOOST program should report no malfunctioning opcodes when run in test mode; it should only output a single value, the BOOST keycode. _What BOOST keycode does it produce?_
"""


def debug(func):
    """Debug utility function."""
    def wrapper(*args):
        if os.getenv("DEBUG"):
            logging.debug(f"Registers: {args[0].registers}")
            logging.debug(f"Program: {args[0].program[:20]}")
        return func(*args)
    return wrapper


class Register:

    PC = 0
    EIP = 0
    EBP = 0

    def __repr__(self):
        return f"PC: {self.PC}, EIP: {self.EIP}, EBP: {self.EBP}"


class Program:

    def __init__(self, data: str, memory_size: int = 1024, inputs: List[int] = []):
        self.data = data
        self.program = [int(instruction) for instruction in self.data.split(',')]
        self.inputs = iter(inputs)
        self.outputs = list()
        self.memory_size = memory_size
        self.registers = Register()
        self.__alloc_memory()

    def __alloc_memory(self) -> None:
        """Expended program memory space for futher data allocation"""

        for _ in range(self.memory_size):
            self.program.append(0)

    def execute(self):
        """Start program execution """

        logging.debug(f"Allocate additional memory of size: {self.memory_size}, program: {self.data}")
        self.__alloc_memory()

        logging.debug(f"Start program!")
        while True:
            try:
                opcode, modes = self.__parse_current_instruction()
                self.__execute_opcode(opcode, modes)
            except StopIteration:
                break

        logging.debug(f"End program execution: output: {''.join([str(i) for i in self.outputs])}")

    def __halt(self) -> Exception: raise StopIteration("Halt program: opcode: 99")

    def __load_r(self, mode: int, addr: int) -> int:
        """Load instruction parameters, for read parameters modes are:
        0: immediate => read value pointed at the address of the paremeter
        1: position => value of the parameter is the parameter
        2: relative => read value pointed at the address of the parameter + EBP"""

        if mode == 0:
            return self.program[self.program[addr]]
        elif mode == 1:
            return self.program[addr]
        elif mode == 2:
            return self.program[self.registers.EBP + self.program[addr]]
        else:
            raise ValueError(f"Unknown mode:{mode}!")

    def __load_w(self, mode: int, index: int) -> int:
        """Load instruction parameters, for write parameters, modes are:
        0: immediate => read value pointed at the address of the paremeter
        2: relative => read value pointed at the address of the parameter + EBP"""

        if mode == 0:
            return self.program[index]
        elif mode == 2:
            return self.program[index] + self.registers.EBP
        else:
            raise ValueError(f"Unknown mode:{mode}!")

    def __store(self, value: int, x_addr: int) -> None:
        """Store value at the given address of the program."""

        self.program[x_addr] = value

    def __add(self, x: int, y: int, dest: int) -> None:
        """opcode: `01`, PC: `4`, add x and y and store the value in dest."""
        self.__store(x + y, dest)
        self.registers.EIP += 4

    def __mul(self, x: int, y: int, dest: int) -> None:
        """opcode: `02`, PC: `4`, multiply x and y and store the value in dest."""

        self.__store(x * y, dest)
        self.registers.EIP += 4

    def stdin(self, dest: int) -> None:
        """opcode: `03`, PC: `2`, get stdin and store it to dest."""

        self.__store(int(next(self.inputs)), dest)
        self.registers.EIP += 2

    def stdout(self, value: int) -> None:
        """opcode: `04`, PC: `2`, stdout value."""

        self.outputs.append(value)
        self.registers.EIP += 2
        logging.info(f">&1 {value}")

    def __jz(self, x: int, addr: int) -> None:
        """opcode: `05`, PC: `3`, jump to addr if x is non-zero else it does nothing."""

        if x:
            self.registers.EIP = addr
        else:
            self.registers.EIP += 3

    def __jnz(self, x: int, addr: int) -> None:
        """opcode: `06`, PC: `3`, jump to addr if x is zero else it does nothing."""

        if not x:
            self.registers.EIP = addr
        else:
            self.registers.EIP += 3

    def __lt(self, x: int, y: int, dest: int) -> None:
        """opcode: `07`, PC: `4`, Compare if x lower then y write `1` to dest else `0`."""

        if x < y:
            self.__store(1, dest)
        else:
            self.__store(0, dest)
        self.registers.EIP += 4

    def __eq(self, x: int, y: int, dest: int) -> None:
        """opcode: `08`, PC: `4` if x equal to y then write `1` to dest else `0`."""

        if x == y:
            self.__store(1, dest)
        else:
            self.__store(0, dest)

        self.registers.EIP += 4

    def __sb(self, offset: int) -> None:
        """opcode: `09`, Move register `EBP` by the value of offset (could be negative)."""

        self.registers.EBP += offset
        self.registers.EIP += 2

    def __parse_current_instruction(self) -> Tuple[int, List[int]]:
        """Parse current instruction."""

        instruction = self.program[self.registers.EIP]
        opcode = instruction % 100
        modes = [
            instruction // 100 % 10,
            instruction // 1000 % 10,
            instruction // 10000 % 10
        ]

        return opcode, modes

    @debug
    def __execute_opcode(self, opcode: int, modes: list) -> None:
        """Execute opcode """

        logging.debug(f"Going to execute opcode: {opcode}")

        if opcode == 1:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            y = self.__load_r(modes[1], self.registers.EIP + 2)
            dest = self.__load_w(modes[2], self.registers.EIP + 3)
            self.__add(x, y, dest)
        elif opcode == 2:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            y = self.__load_r(modes[1], self.registers.EIP + 2)
            dest = self.__load_w(modes[2], self.registers.EIP + 3)
            self.__mul(x, y, dest)
        elif opcode == 3:
            dest = self.__load_w(modes[0], self.registers.EIP + 1)
            self.stdin(dest)
        elif opcode == 4:
            value = self.__load_r(modes[0], self.registers.EIP + 1)
            self.stdout(value)
        elif opcode == 5:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            addr = self.__load_r(modes[1], self.registers.EIP + 2)
            self.__jz(x, addr)
        elif opcode == 6:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            addr = self.__load_r(modes[1], self.registers.EIP + 2)
            self.__jnz(x, addr)
        elif opcode == 7:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            y = self.__load_r(modes[1], self.registers.EIP + 2)
            dest = self.__load_w(modes[2], self.registers.EIP + 3)
            self.__lt(x, y, dest)
        elif opcode == 8:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            y = self.__load_r(modes[1], self.registers.EIP + 2)
            dest = self.__load_w(modes[2], self.registers.EIP + 3)
            self.__eq(x, y, dest)
        elif opcode == 9:
            offset = self.__load_r(modes[0], self.registers.EIP + 1)
            self.__sb(offset)
        elif opcode == 99:
            self.__halt()
        else:
            raise ValueError(f"Invalid opcode: {opcode}, {self.registers}")


def resolve():
    logging.info(f"Start {__name__}")
    program_data = open("resources/day9-input.txt", "r").read()
    prog = Program(program_data, 4096, [1])
    prog.execute()
