# coding: utf-8

import os
import sys
import time
import logging
import traceback
from copy import deepcopy
from typing import Tuple, List
from queue import Queue
from threading import Thread


"""
# --- Day 13: Care Package ---

As you ponder the solitude of space and the ever-increasing three-hour roundtrip for messages between you and Earth, you notice that the Space Mail Indicator Light is blinking. To help keep you sane, the Elves have sent you a care package.

It's a new game for the ship's [arcade cabinet](https://en.wikipedia.org/wiki/Arcade_cabinet)! Unfortunately, the arcade is _all the way_ on the other end of the ship. Surely, it won't be hard to build your own - the care package even comes with schematics.

The arcade cabinet runs [Intcode](9) software like the game the Elves sent (your puzzle input). It has a primitive screen capable of drawing square _tiles_ on a grid. The software draws tiles to the screen with output instructions: every three output instructions specify the `` x `` position (distance from the left), `` y `` position (distance from the top), and `` tile id ``. The `` tile id `` is interpreted as follows:

*   `` 0 `` is an _empty_ tile. No game object appears in this tile.
*   `` 1 `` is a _wall_ tile. Walls are indestructible barriers.
*   `` 2 `` is a _block_ tile. Blocks can be broken by the ball.
*   `` 3 `` is a _horizontal paddle_ tile. The paddle is indestructible.
*   `` 4 `` is a _ball_ tile. The ball moves diagonally and bounces off objects.

For example, a sequence of output values like `` 1,2,3,6,5,4 `` would draw a _horizontal paddle_ tile (`` 1 `` tile from the left and `` 2 `` tiles from the top) and a _ball_ tile (`` 6 `` tiles from the left and `` 5 `` tiles from the top).

Start the game. _How many block tiles are on the screen when the game exits?_


The game didn't run because you didn't put in any quarters. Unfortunately, you did not bring any quarters. Memory address 0 represents the number of quarters that have been inserted; set it to 2 to play for free.

The arcade cabinet has a joystick that can move left and right. The software reads the position of the joystick with input instructions:

If the joystick is in the neutral position, provide 0.
If the joystick is tilted to the left, provide -1.
If the joystick is tilted to the right, provide 1.
The arcade cabinet also has a segment display capable of showing a single number that represents the player's current score. When three output instructions specify X=-1, Y=0, the third output instruction is not a tile; the value instead specifies the new score to show in the segment display. For example, a sequence of output values like -1,0,12345 would show 12345 as the player's current score.

Beat the game by breaking all the blocks. What is your score after the last block is broken?
"""


def debug(func):
    """Debug utility function."""
    def wrapper(*args):
        if os.getenv("DEBUG"):
            logging.debug(f"Registers: {args[0].registers}")
            logging.debug(f"Program: {args[0].program[:20]}")
        return func(*args)
    return wrapper


class Register:

    PC = 0
    EIP = 0
    EBP = 0

    def __repr__(self):
        return f"PC: {self.PC}, EIP: {self.EIP}, EBP: {self.EBP}"


class Program(Thread):

    def __init__(self, data: str, inputs: Queue, outputs: Queue, memory_size: int = 1024):
        super(Program, self).__init__()
        self.data = data
        self.status = "idle"
        self.program = [int(instruction) for instruction in self.data.split(',')]
        self.program[0] = 2
        self.inputs = inputs
        self.outputs = outputs
        self.memory_size = memory_size
        self.registers = Register()
        self.__alloc_memory()

    def __alloc_memory(self) -> None:
        """Expended program memory space for futher data allocation"""

        for _ in range(self.memory_size):
            self.program.append(0)

    def run(self) -> None:
        """Start program execution """

        logging.debug(f"Allocate additional memory of size: {self.memory_size}, program: {self.data}")
        logging.debug(f"Start program!")
        self.status = "running"

        while True:
            try:
                opcode, modes = self.__parse_current_instruction()
                self.__execute_opcode(opcode, modes)
            except StopIteration:
                break

        logging.debug(f"End program execution: output: {self.outputs.qsize()}")

    def __halt(self) -> Exception:
        self.status = "halt"
        raise StopIteration("Halt program: opcode: 99")

    def __load_r(self, mode: int, addr: int) -> int:
        """Load instruction parameters, for read parameters modes are:
        0: immediate => read value pointed at the address of the paremeter
        1: position => value of the parameter is the parameter
        2: relative => read value pointed at the address of the parameter + EBP"""

        if mode == 0:
            return self.program[self.program[addr]]
        elif mode == 1:
            return self.program[addr]
        elif mode == 2:
            return self.program[self.registers.EBP + self.program[addr]]
        else:
            raise ValueError(f"Unknown mode:{mode}!")

    def __load_w(self, mode: int, index: int) -> int:
        """Load instruction parameters, for write parameters, modes are:
        0: immediate => read value pointed at the address of the paremeter
        2: relative => read value pointed at the address of the parameter + EBP"""

        if mode == 0:
            return self.program[index]
        elif mode == 2:
            return self.program[index] + self.registers.EBP
        else:
            raise ValueError(f"Unknown mode:{mode}!")

    def __store(self, value: int, x_addr: int) -> None:
        """Store value at the given address of the program."""

        self.program[x_addr] = value

    def __add(self, x: int, y: int, dest: int) -> None:
        """opcode: `01`, PC: `4`, add x and y and store the value in dest."""
        self.__store(x + y, dest)
        self.registers.EIP += 4

    def __mul(self, x: int, y: int, dest: int) -> None:
        """opcode: `02`, PC: `4`, multiply x and y and store the value in dest."""

        self.__store(x * y, dest)
        self.registers.EIP += 4

    def stdin(self, dest: int) -> None:
        """opcode: `03`, PC: `2`, get stdin and store it to dest."""

        self.__store(int(self.inputs.get()), dest)
        self.registers.EIP += 2

    def stdout(self, value: int) -> None:
        """opcode: `04`, PC: `2`, stdout value."""

        self.outputs.put(value)
        self.registers.EIP += 2
        logging.debug(f">&1 {value}")

    def __jz(self, x: int, addr: int) -> None:
        """opcode: `05`, PC: `3`, jump to addr if x is non-zero else it does nothing."""

        if x:
            self.registers.EIP = addr
        else:
            self.registers.EIP += 3

    def __jnz(self, x: int, addr: int) -> None:
        """opcode: `06`, PC: `3`, jump to addr if x is zero else it does nothing."""

        if not x:
            self.registers.EIP = addr
        else:
            self.registers.EIP += 3

    def __lt(self, x: int, y: int, dest: int) -> None:
        """opcode: `07`, PC: `4`, Compare if x lower then y write `1` to dest else `0`."""

        if x < y:
            self.__store(1, dest)
        else:
            self.__store(0, dest)
        self.registers.EIP += 4

    def __eq(self, x: int, y: int, dest: int) -> None:
        """opcode: `08`, PC: `4` if x equal to y then write `1` to dest else `0`."""

        if x == y:
            self.__store(1, dest)
        else:
            self.__store(0, dest)

        self.registers.EIP += 4

    def __sb(self, offset: int) -> None:
        """opcode: `09`, Move register `EBP` by the value of offset (could be negative)."""

        self.registers.EBP += offset
        self.registers.EIP += 2

    def __parse_current_instruction(self) -> Tuple[int, List[int]]:
        """Parse current instruction."""

        instruction = self.program[self.registers.EIP]
        opcode = instruction % 100
        modes = [
            instruction // 100 % 10,
            instruction // 1000 % 10,
            instruction // 10000 % 10
        ]

        return opcode, modes

    @debug
    def __execute_opcode(self, opcode: int, modes: list) -> None:
        """Execute opcode """

        logging.debug(f"Going to execute opcode: {opcode}")

        if opcode == 1:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            y = self.__load_r(modes[1], self.registers.EIP + 2)
            dest = self.__load_w(modes[2], self.registers.EIP + 3)
            self.__add(x, y, dest)
        elif opcode == 2:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            y = self.__load_r(modes[1], self.registers.EIP + 2)
            dest = self.__load_w(modes[2], self.registers.EIP + 3)
            self.__mul(x, y, dest)
        elif opcode == 3:
            dest = self.__load_w(modes[0], self.registers.EIP + 1)
            self.stdin(dest)
        elif opcode == 4:
            value = self.__load_r(modes[0], self.registers.EIP + 1)
            self.stdout(value)
        elif opcode == 5:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            addr = self.__load_r(modes[1], self.registers.EIP + 2)
            self.__jz(x, addr)
        elif opcode == 6:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            addr = self.__load_r(modes[1], self.registers.EIP + 2)
            self.__jnz(x, addr)
        elif opcode == 7:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            y = self.__load_r(modes[1], self.registers.EIP + 2)
            dest = self.__load_w(modes[2], self.registers.EIP + 3)
            self.__lt(x, y, dest)
        elif opcode == 8:
            x = self.__load_r(modes[0], self.registers.EIP + 1)
            y = self.__load_r(modes[1], self.registers.EIP + 2)
            dest = self.__load_w(modes[2], self.registers.EIP + 3)
            self.__eq(x, y, dest)
        elif opcode == 9:
            offset = self.__load_r(modes[0], self.registers.EIP + 1)
            self.__sb(offset)
        elif opcode == 99:
            self.__halt()
        else:
            raise ValueError(f"Invalid opcode: {opcode}, {self.registers}")


class Tile:

    def __init__(self, x, y, symbol):
        self.x = x
        self.y = y
        self.symbol = symbol


class Empty(Tile):
    def __init__(self, x, y):
        super(Empty, self).__init__(x, y, " ")

    def __repr__(self):
        return f"[Empty]:({self.x},{self.y})"


class Block(Tile):
    def __init__(self, x, y):
        super(Block, self).__init__(x, y, u"\u2588")

    def __repr__(self):
        return f"[Block]:({self.x},{self.y})"


class Wall(Tile):
    def __init__(self, x, y):
        super(Wall, self).__init__(x, y, u"\u2503")

    def __repr__(self):
        return f"[Wall]:({self.x},{self.y})"


class Paddle:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.symbol = u"\u2501"

    def move(self, movement):
        pass

    def __repr__(self):
        return f"[Paddle]:({self.x},{self.y})"


class Game:

    def __init__(self, inputs_q: Queue):
        self.inputs = inputs_q
        self.score = 0
        self.tiles = list()
        self.paddle = None
        self.ball = None
        self.status = "creation"

    def insert(self, tile):
        if tile.y > len(self.tiles) - 1:
            for _ in range(len(self.tiles), tile.y + 1):
                self.tiles.append([])

        self.tiles[tile.y].append(tile)

        # Sorted tile of a raw by x
        self.tiles[tile.y].sort(key=lambda x: x.x)

    def append(self, tile):
        if isinstance(tile, Wall):
            self.insert(tile)
        elif isinstance(tile, Block):
            self.insert(tile)
        elif isinstance(tile, Empty):
            self.insert(tile)
        elif isinstance(tile, Ball):
            self.ball = time
        elif isinstance(tile, Paddle):
            self.paddle = tile

    def live_map(self):
        self.print_map()
        [sys.stdout.write("\033c") for _ in range(len(self.tiles))]

    def print_map(self):

        tiles = deepcopy(self.tiles)
        tiles[self.ball.y].insert(self.ball.x, self.ball)
        tiles[self.paddle.y].insert(self.paddle.x, self.paddle)

        tmp_map = ""
        for y in tiles:
            tmp_map += "".join([tile.symbol for tile in y])
            tmp_map += "\n"

        print(f"Score: {self.score}")
        print(tmp_map)
        del tiles

    def create_tile(self, item: list):

        tile = None
        if int(item[2]) == 0 and item[0] != -1:
            tile = Empty(item[0], item[1])
        elif int(item[2]) == 1:
            tile = Wall(item[0], item[1])
        elif int(item[2]) == 2:
            tile = Block(item[0], item[1])
        elif int(item[2]) == 3:
            self.paddle = Paddle(item[0], item[1])
        elif int(item[2]) == 4:
            self.ball = Ball(item[0], item[1])
        elif item[0] == -1 and item[1] == 0:
            self.score = int(item[2])
            self.status = "start"
            part1(self)
            for y, raw in enumerate(self.tiles.copy()):
                if not raw:
                    if self.ball.y != y and self.paddle.y != y and len(self.tiles) > y:
                        del self.tiles[y]
            self.inputs.put(0)
        else:
            raise ValueError(f"Unknown tile id: {item[0]}")

        if tile is not None:
            self.append(tile)

    def update_tile(self, item: list):

        tile = None
        if int(item[2]) == 0 and item[0] != -1:
            tile = Empty(item[0], item[1])
        elif int(item[2]) == 1:
            tile = Wall(item[0], item[1])
        elif int(item[2]) == 2:
            tile = Block(item[0], item[1])
        elif int(item[2]) == 3:
            self.paddle.x = item[0]
            self.paddle.y = item[1]
        elif int(item[2]) == 4:
            self.move_joystick(item[0])
            self.ball.x = item[0]
            self.ball.y = item[1]
        elif item[0] == -1 and item[1] == 0:
            self.score = int(item[2])

        if tile:
            self.tiles[item[1]][item[0]] = tile

    def move_joystick(self, new_x):
        if new_x > self.paddle.x:
            # logging.warn("move paddle right")
            self.inputs.put(1)
        if new_x < self.paddle.x:
            # logging.warn("move paddle left")
            self.inputs.put(-1)
        if new_x == self.paddle.x:
            # logging.warn("don't move paddle")
            self.inputs.put(0)


class Ball:

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.symbol = u"\u29BF"

    def __repr__(self):
        return f"[Ball]:({self.x},{self.y})"

    def __str__(self):
        return self.__repr__()


def part1(game):
    counter = 0
    for y in game.tiles:
        for x in y:
            if isinstance(x, Block):
                counter += 1

    logging.info(f"Solution of day 13 part #1: {counter}")


def resolve():
    logging.info(f"Start {__name__}")

    program_data = open('resources/day13-input.txt', 'r').read()
    inputs, outputs = Queue(), Queue()
    prog = Program(program_data, inputs, outputs, 4096)
    prog.start()

    item = list()
    game = Game(inputs)

    logging.info("Build Game Map!")
    prev_score = 0
    all_score = []
    while prog.status != "halt":
        conf = prog.outputs.get()
        item.append(conf)
        if game.status != "start":
            if len(item) == 3:
                game.create_tile(item)
                item.clear()

        if game.status == "start":
            if len(item) == 3:
                game.update_tile(item)
                item.clear()
                game.live_map()

    _queue_size = prog.outputs.qsize()
    for i in range(_queue_size):
        item.append(prog.outputs.get())
        if len(item) == 3:
            game.update_tile(item)
            item.clear()

    game.print_map()
    logging.info(f"Solution of day 13 part #2: {game.score}")


if __name__ == "__main__":
    resolve()
