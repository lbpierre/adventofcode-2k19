# coding: utf-8

import logging


"""
# --- Day 2: 1202 Program Alarm ---

On the way to your [gravity assist](https://en.wikipedia.org/wiki/Gravity_assist) around the Moon, your ship computer beeps angrily about a "[1202 program alarm](https://www.hq.nasa.gov/alsj/a11/a11.landing.html#1023832)". On the radio, an Elf is already explaining how to handle the situation: "Don't worry, that's perfectly norma--" The ship computer [bursts into flames](https://en.wikipedia.org/wiki/Halt_and_Catch_Fire).

You notify the Elves that the computer's [magic smoke](https://en.wikipedia.org/wiki/Magic_smoke) seems to have <span title="Looks like SOMEONE forgot to change the switch to 'more magic'.">escaped</span>. "That computer ran _Intcode_ programs like the gravity assist program it was working on; surely there are enough spare parts up there to build a new Intcode computer!"

An Intcode program is a list of [integers](https://en.wikipedia.org/wiki/Integer) separated by commas (like `` 1,0,0,3,99 ``). To run one, start by looking at the first integer (called position `` 0 ``). Here, you will find an _opcode_ - either `` 1 ``, `` 2 ``, or `` 99 ``. The opcode indicates what to do; for example, `` 99 `` means that the program is finished and should immediately halt. Encountering an unknown opcode means something went wrong.

Opcode `` 1 `` _adds_ together numbers read from two positions and stores the result in a third position. The three integers _immediately after_ the opcode tell you these three positions - the first two indicate the _positions_ from which you should read the input values, and the third indicates the _position_ at which the output should be stored.

For example, if your Intcode computer encounters `` 1,10,20,30 ``, it should read the values at positions `` 10 `` and `` 20 ``, add those values, and then overwrite the value at position `` 30 `` with their sum.

Opcode `` 2 `` works exactly like opcode `` 1 ``, except it _multiplies_ the two inputs instead of adding them. Again, the three integers after the opcode indicate _where_ the inputs and outputs are, not their values.

Once you're done processing an opcode, _move to the next one_ by stepping forward `` 4 `` positions.

For example, suppose you have the following program:

    1,9,10,3,2,3,11,0,99,30,40,50

For the purposes of illustration, here is the same program split into multiple lines:

    1,9,10,3,
    2,3,11,0,
    99,
    30,40,50

The first four integers, `` 1,9,10,3 ``, are at positions `` 0 ``, `` 1 ``, `` 2 ``, and `` 3 ``. Together, they represent the first opcode (`` 1 ``, addition), the positions of the two inputs (`` 9 `` and `` 10 ``), and the position of the output (`` 3 ``). To handle this opcode, you first need to get the values at the input positions: position `` 9 `` contains `` 30 ``, and position `` 10 `` contains `` 40 ``. _Add_ these numbers together to get `` 70 ``. Then, store this value at the output position; here, the output position (`` 3 ``) is _at_ position `` 3 ``, so it overwrites itself. Afterward, the program looks like this:

<pre><code>1,9,10,<em>70</em>,
2,3,11,0,
99,
30,40,50
</code></pre>

Step forward `` 4 `` positions to reach the next opcode, `` 2 ``. This opcode works just like the previous, but it multiplies instead of adding. The inputs are at positions `` 3 `` and `` 11 ``; these positions contain `` 70 `` and `` 50 `` respectively. Multiplying these produces `` 3500 ``; this is stored at position `` 0 ``:

<pre><code><em>3500</em>,9,10,70,
2,3,11,0,
99,
30,40,50
</code></pre>

Stepping forward `` 4 `` more positions arrives at opcode `` 99 ``, halting the program.

Here are the initial and final states of a few more small programs:

*   `` 1,0,0,0,99 `` becomes <code><em>2</em>,0,0,0,99</code> (`` 1 + 1 = 2 ``).
*   `` 2,3,0,3,99 `` becomes <code>2,3,0,<em>6</em>,99</code> (`` 3 * 2 = 6 ``).
*   `` 2,4,4,5,99,0 `` becomes <code>2,4,4,5,99,<em>9801</em></code> (`` 99 * 99 = 9801 ``).
*   `` 1,1,1,4,99,5,6,0,99 `` becomes <code><em>30</em>,1,1,4,<em>2</em>,5,6,0,99</code>.

Once you have a working computer, the first step is to restore the gravity assist program (your puzzle input) to the "1202 program alarm" state it had just before the last computer caught fire. To do this, _before running the program_, replace position `` 1 `` with the value `` 12 `` and replace position `` 2 `` with the value `` 2 ``. _What value is left at position `` 0 ``_ after the program halts?
"""


def execute_program(data):
    data_index = [int(i) for i in data.split(',')]

    def store(value, x_addr): data_index[x_addr] = value
    def stopIteration(info): raise StopIteration(info)
    add = lambda x_addr, y_addr, dest: store(load(x_addr) + load(y_addr), dest)
    mul = lambda x_addr, y_addr, dest: store(load(x_addr) * load(y_addr), dest)
    load = lambda x_addr: data_index[x_addr]
    funcs = (None, add, mul, load, store)
    execute_opcode = lambda x, *args: funcs[x](*args) if x != 99 else stopIteration("Exit op code reach")

    logging.debug("Loading programm executor")
    logging.debug(f"Execute: {data}")
    for instruction in range(0, len(data_index), 4):
        if instruction + 4 > len(data_index):
            if data_index[instruction] != 99:
                logging.error(f"End execution instruction is too short: {data_index[instruction:]}")
            break
        try:
            execute_opcode(data_index[instruction],
                           data_index[instruction + 1],
                           data_index[instruction + 2],
                           data_index[instruction + 3])
        except StopIteration:
            break

    output = ",".join([str(index) for index in data_index])
    logging.info(f"Program: {data} -> {output}")
    return output


def resolve():

    assert execute_program("1,9,10,3,2,3,11,0,99,30,40,50").split(',')[0] == '3500'
    assert execute_program('1,0,0,0,99') == '2,0,0,0,99'
    assert execute_program('2,3,0,3,99') == '2,3,0,6,99'
    assert execute_program('1,1,1,4,99,5,6,0,99') == '30,1,1,4,2,5,6,0,99'

    with open('resources/day2-input.txt', 'r') as f:
        program = f.read()
        program = program.replace('x', '12')
        program = program.replace('y', '2')

    logging.info(f"Solution of day 2: {execute_program(program)}")
